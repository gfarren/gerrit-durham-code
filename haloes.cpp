#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "interpolation.h"
#include "integration.h"
#include <boost/math/special_functions/erf.hpp>
#include <fstream>
#include <sstream>
#include <string>
#include <iterator>
#include <random>
#include <algorithm>
#include "helpers.hpp"
#include "model.hpp"
#include "haloes.hpp"
#include "galaxies.hpp"
#include "readers.hpp"

namespace haloes{

	Halo::Halo(double mass, double z_in, Halo *parent_halo, helpers::Interpolator *interp){
		this->m=mass;
		this->z=z_in;
		this->parent=parent_halo;
		this->c=mass_concentration_relation_1(mass, z_in, interp);	
	}

	Halo::Halo(double mass, double z_in, helpers::Interpolator *interp){
		this->m=mass;
		this->z=z_in;
		this->parent=NULL;
		this->c=mass_concentration_relation_1(mass, z_in, interp);
	}

	Halo::Halo(double mass, double z_in, Halo *parent_halo){
		this->m=mass;
		this->z=z_in;
		this->parent=parent_halo;
		this->c=7.0;	
	}

	Halo::Halo(double mass, double z_in){
		this->m=mass;
		this->z=z_in;
		this->parent=NULL;
		this->c=7.0;
	}

	std::vector<double> Halo::get_mah(){
		if (this->most_massive_progenitor != NULL){
			std::vector<double> previousMasses;
			previousMasses = this->most_massive_progenitor->get_mah();
			previousMasses.push_back(this->m);
			return previousMasses;
		}else{
			std::vector<double> firstMass;
			firstMass.push_back(this->m);
			return firstMass;
		}
	}
	std::vector<Halo *> Halo::get_halo_history(){
		if (this->most_massive_progenitor != NULL){
			std::vector<Halo *> previousHaloes;
			previousHaloes = this->most_massive_progenitor->get_halo_history();
			previousHaloes.push_back(this);
			return previousHaloes;
		}else{
			std::vector<Halo *> firstHalo;
			firstHalo.push_back(this);
			return firstHalo;
		}
	}

	std::vector<double> Halo::get_z_mah(){
		if (this->most_massive_progenitor != NULL){
			std::vector<double> previousZ;
			previousZ = this->most_massive_progenitor->get_z_mah();
			previousZ.push_back(this->z);
			return previousZ;
		}else{
			std::vector<double> firstZ;
			firstZ.push_back(this->z);
			return firstZ;
		}
	}
	void Halo::populate_progenitors(double deltaW, double max_z, double min_mass, helpers::Interpolator *interp, bool populateChildren, bool useJeansMass){

		double sumMass,zp,d,sigma_sq_current,r,dSigma_sq,mass;

		sumMass=0.0;
		this->progenitors.clear();
		this->most_massive_progenitor=NULL;

		d=interp->d_of_z(this->z);
		zp=interp->z_of_d(1/(deltaW/1.686+1/d));
		sigma_sq_current=interp->get_sigma_sq(this->m);

		if (useJeansMass){
			min_mass=model::Cosmology::jeans_mass(this->z);
		}

		if(zp>max_z && !useJeansMass){
			return;
		}

		do{
			r = ((double) rand() / (RAND_MAX));
			dSigma_sq=Halo::ps_cdf_inverse(r, deltaW);
			mass=interp->get_sigma_sq_inverse(sigma_sq_current+dSigma_sq);
			if (this->m-sumMass>=mass){
				sumMass+=mass;
				if (mass>=min_mass){
					Halo *p = new Halo(mass, zp, this, interp);
					if (this->most_massive_progenitor == NULL || mass>this->most_massive_progenitor->get_mass()){
						this->most_massive_progenitor=p;
					}
					if (populateChildren){
						(*p).populate_progenitors(deltaW, max_z, min_mass, interp, true, useJeansMass);
						this->progenitors.push_back(p);
					}
					
				}
			}
		}while(this->m-sumMass>=min_mass);

		if(!populateChildren && this->most_massive_progenitor != NULL){
			most_massive_progenitor->populate_progenitors(deltaW, max_z, min_mass, interp, false, useJeansMass);
			this->progenitors.push_back(this->most_massive_progenitor);
		}
		
	}

	std::vector<Halo *> Halo::get_leafs(){
		if (this->progenitors.size()==0){
			std::vector<Halo *> vec = std::vector<Halo *>();
			vec.push_back(this);
			return vec;
		}else{
			std::vector<Halo *> vec = this->progenitors[0]->get_leafs();
			for(int i=1; i<this->progenitors.size(); i++){
				std::vector<Halo *> leafs = this->progenitors[i]->get_leafs();
				vec.insert(vec.end(), leafs.begin(),leafs.end()); 
			}
			return vec;
		}
	}

	double Halo::get_smooth_mass_growth(){
		double mass=this->get_mass();
		for(int i=0; i< progenitors.size(); i++){
			mass-=progenitors[i]->get_mass();
		}
		return mass;
	}

	double Halo::get_life_time(){
		Halo *parent=this;
		while(parent->get_parent() != NULL && parent->get_mass() < 2.0*this->get_mass()){
			parent=parent->get_parent();
		}
		return parent->get_z();
	}

	void Halo::set_parent(Halo *parent){
		parent->add_progenitor(this);
	}

	void Halo::add_progenitor(Halo *progenitor){
		if(std::find(progenitors.begin(), progenitors.end(), progenitor) == progenitors.end()){
			progenitors.push_back(progenitor);
			if (most_massive_progenitor==NULL || progenitor->get_mass()>most_massive_progenitor->get_mass()){
				most_massive_progenitor=progenitor;
			}
			progenitor->set_parent(this);
		}
	}

	double Halo::ps_cdf_inverse(double r, double deltaW){
		return pow(deltaW, 2.0)/2/pow(boost::math::erf_inv(1-r), 2.0);
	}

	double Halo::delta_c(double c){
		return 200.0/3.0*pow(c, 3.0)/(log(1+c)-c/(1+c));
	}

	double Halo::get_T200(double m200, double z, double mu){
		return mu*model::M_proton*pow(get_V200(m200, z),2)/(2*model::Kb);
		
	}
	double Halo::get_R200(double m200, double z){
		double m_g = m200*model::M_sun;
		return pow(m_g/(4.0/3.0*M_PI*200.0*model::Cosmology::rho_crit_z(z)), 1.0/3.0);
	}
	double Halo::get_V200(double m200, double z){
		double m_g = m200*model::M_sun;
		double r200 = get_R200(m200, z);

		return sqrt(model::G*m_g/r200);
	}
	double Halo::get_binding_energy(double m200, double z){
		return m200*model::M_sun*pow(get_V200(m200, z),2)*model::Cosmology::omegaB/model::Cosmology::omegaM/4;
	}

	double Halo::get_Mdm_tilde(double r, double c){
		return 3*delta_c(c)/(pow(c, 3)*200.0)*(log(1.0+c*r)-c*r/(1.0+c*r));
	}
	double Halo::get_rhoB_tilde(double r, double c, double temp_ratio){ //after reionization need to be able to set halo temperature (solution depends on the ration of virial temperature to halo temperature)
		return pow(1.0+c*r, 6.0*temp_ratio*delta_c(c)/(pow(c, 3)*r*200.0));
	}
	double Halo::get_rhoB_tilde_max(double c, double temp_ratio){ //after reionization need to be able to set halo temperature (solution depends on the ration of virial temperature to halo temperature)
		return exp(6.0*temp_ratio*delta_c(c)/(pow(c, 2)*200.0));
	}
	double Halo::get_Mb_tilde(double r, double c, double temp_ratio){
		alglib::autogkstate gkState;
		alglib::autogkreport gkRep;
		alglib::autogksmooth(0.0, r, gkState);
		double integral;
		std::vector<double *> params{&c, &temp_ratio};
		alglib::autogkintegrate(gkState, mB_integrant, &params);
		autogkresults(gkState, integral, gkRep);

		return 3/200.0*model::Cosmology::omegaB*integral;
	}

	void Halo::mB_integrant(double x, double xminusa, double bminusx, double &y, void *ptr){
		std::vector<double *> params = *((std::vector<double *> *)ptr);
		double c = *params[0];
		double temp_ratio = *params[1];
		y=pow(x, 2)*get_rhoB_tilde(x, c, temp_ratio);
	}

	//Ludlow et al. 2016 (with some adjustments)
	double Halo::mass_concentration_relation_1(double m200, double z, helpers::Interpolator *interp){

		double a=1/(1+z);
		double nu_0;
		if(a>0.15744646129941298){
			nu_0=(4.135-0.564/a-0.21*pow(a, -2)+0.0557*pow(a, -3)-0.00348*pow(a, -4))/interp->d_of_z(z);
		}else{
			nu_0=0.6895174822730894*exp(3.3506589471122084*(a-0.15744646129941298))/interp->d_of_z(z);
			//double a_tmp=0.15414170170378644;
			//nu_0=(4.135-0.564/a_tmp-0.21*pow(a_tmp, -2)+0.0557*pow(a_tmp, -3)-0.00348*pow(a_tmp, -4))/interp->d_of_z(z);
		}
		double beta=0.307*pow(a, -0.54);
		double c_0=3.395*pow(a, 0.215);
		double gamma_1=0.628*pow(a, 0.047);
		double gamma_2=0.317*pow(a, 0.893);

		double nu=model::Cosmology::delta_crit/sqrt(interp->get_sigma_sq(m200, z));

		//std::cout<<a<<"; "<<nu_0<<"; "<<beta<<"; "<<c_0<<"; "<<gamma_1<<"; "<<gamma_2<<"; "<<nu<<std::endl;

		return c_0*pow(nu/nu_0, -gamma_1)*pow(1+pow(nu/nu_0, 1/beta), -beta*(gamma_2-gamma_1));
	}

	//Correa et al. 2013
	double Halo::mass_concentration_relation_2(double m200, double z){

		double log_c;
		if (z<=4){
			double alpha=1.62774-0.2458*(1+z)+0.01716*pow(1+z,2);
			double beta=1.66079+0.00359*(1+z)-1.6901*pow(1+z,0.00417);
			double gamma=-0.02049+0.0253*pow(1+z,-0.1044);

			log_c=alpha+beta*log10(m200)*(1+gamma*pow(log10(m200), 2));
		}else{
			double alpha=1.226-0.1009*(1+z)+0.00378*pow(1+z,2);
			double beta=0.008634-0.08814*pow(1+z,-0.58816);

			log_c=alpha+beta*log10(m200);
		}

		return pow(10, log_c);
	}

}

