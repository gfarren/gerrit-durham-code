#ifndef Galaxies_H
#define Galaxies_H

namespace galaxies{
	class Galaxy{
		private:
			std::vector<haloes::Halo *> halo_history;
			std::vector<double> cold_gas_history, hot_gas_history, r_cool_history, stellar_mass_history, merged_dm_mass_history, e_radiated_history;
			double cold_gas, hot_gas, stellar_mass, merged_dm_mass, e_radiated, r_cool, ejected_gas;
			std::vector<Galaxy *> satellites = std::vector<Galaxy *>();
			std::vector<Galaxy *> merged_galaxies = std::vector<Galaxy *>();
			helpers::Interpolator *interp;
			model::ModelParams *params;
			double z_became_satellite,z_merged,z_will_merge;
			bool is_satellite=false;
			bool is_merged=false;
			std::default_random_engine generator;
  			std::normal_distribution<double> orbital_parameter_distribution;

  			static void luminosity_integrant(double r_tilde, double xminusa, double bminusx, double &y, void *ptr);
  			static double r_cool_solver(double log_r, void *params);
  			void intial_gas(double dm_mass);
  			void cool_gas(double z0, double z_final, haloes::Halo *halo, bool adaptive=true, bool stars=false);
  			void smoothly_accrete_gas(double smoothly_accreted_dm_mass);
  			void make_stars_do_feedback(double m200, double z);
		public:
			Galaxy(model::ModelParams *params, helpers::Interpolator *interp);

			double get_merged_dm_mass(){return merged_dm_mass;}//dm mass that is not bound in subhaloes
			double halo_dm_mass(){
				return halo_history.front()->get_mass();
			}
			double get_concentration(){
				return halo_history.front()->get_c();
			}
			haloes::Halo* get_halo(){
				return halo_history.front();
			}

			std::vector<Galaxy *> get_satellites(){ return satellites; }
			int get_num_satellites(){
				return (int)satellites.size();
			}
			std::vector<Galaxy *> get_merged_satellites(){ return merged_galaxies; }

			double get_z_became_satellite(){ return z_became_satellite; }
			double get_z_will_merge(){ return z_will_merge; }

			void merge_satellites(double z);
			void becomes_satellite(haloes::Halo *parent_halo);

			bool get_is_satellite(){ return is_satellite; }
			bool get_is_merged(){ return is_merged; }

			void set_is_merged(double z){
				z_merged=z;
				is_merged=true;
				is_satellite=false;
			}

			double get_cold_gas(){return cold_gas;}
			double get_hot_gas(){return hot_gas;}
			double get_stellar_mass(){return stellar_mass;}
			double get_radiated_energy(){return e_radiated;}
			double get_ejected_gas(){return ejected_gas;}

			
			void add_hot_gas(Galaxy *g){
				this->hot_gas+=g->get_hot_gas()+g->get_ejected_gas();
			}
			void add_cold_gas(Galaxy *g){
				this->cold_gas+=g->get_cold_gas();
			}
			void add_stellar_mass(Galaxy *g){
				this->stellar_mass+=g->get_stellar_mass();
			}
			void add_energy_radiated(Galaxy *g){
				this->e_radiated+=g->get_radiated_energy();
			}


			void populate_galaxies(haloes::Halo *halo_in);
			void populate_galaxies_wGas(haloes::Halo *halo_in);
			void populate_galaxies_wStars(haloes::Halo *halo_in);

			static double t_cool(double t_gas, double rho_b_tilde, double z, helpers::Interpolator *interp);
  			static double cool_luminosity(double rmin, double rmax, double m200, double rho_norm, double concentration, double t_gas, double z, helpers::Interpolator *interp);
  			static double t_free_fall(double r0, double m_enclosed_tilde, double m200, double z);
	};
}


#endif