#ifndef Helpers_H
#define Helpers_H

namespace helpers{
	class Interpolator{
		private: 
			alglib::spline1dinterpolant sigma_sq, sigma_sq_inverse, z_of_d_interp, d_of_z_interp, t_of_z_interp, z_of_t_interp, power_spectrum;
			std::vector<alglib::spline2dinterpolant> cooling_tables;
			std::vector<double> zBins_cooling;
			double mink, maxk;
			static std::vector<std::vector<double> > read_file(std::ifstream *file);
			static std::vector<double> convert2vec(std::string str);
			void set_sigma_sq(double minM, double maxM, int steps);
			void set_power_spectrum(std::string filename);
			void set_growth(double minZ, double maxZ, int steps);
			void set_time(double minZ, double maxZ, int steps);
			void set_cooling_table(std::string filename, bool molecules=true);

		public:
			Interpolator(double minZ, double maxZ, int stepsZ);
			Interpolator(double minM, double maxM, int stepsM, double minZ, double maxZ, int stepsZ, std::string power_spectrum_file, std::string cooling_table_file, bool molecules=true);
			Interpolator(double minM, double maxM, int stepsM, double minZ, double maxZ, int stepsZ, std::string power_spectrum_file);
			Interpolator(std::string power_spectrum_file, std::string cooling_table_file, bool molecules=true);
			Interpolator(std::string power_spectrum_file);
			Interpolator();

			double get_sigma_sq_inverse(double sigma_sq){
				return pow(10.0, alglib::spline1dcalc(sigma_sq_inverse, log10(sigma_sq)));
			}
			double get_sigma_sq(double mass){
				return pow(10.0, alglib::spline1dcalc(sigma_sq, log10(mass)));
			}
			double get_sigma_sq(double mass, double z){
				return pow(10.0, alglib::spline1dcalc(sigma_sq, log10(mass)))*pow(d_of_z(z), 2);
			}
			double d_of_z(double z){
				return pow(10.0, alglib::spline1dcalc(d_of_z_interp, z));
			}
			double z_of_d(double d){
				return alglib::spline1dcalc(z_of_d_interp, log10(d));
			}
			double t_of_z(double z){
				return pow(10.0, alglib::spline1dcalc(t_of_z_interp, z));
			}
			double z_of_t(double t){
				return alglib::spline1dcalc(z_of_t_interp, log10(t));
			}
			double get_power_spectrum(double k){
				return pow(10.0, alglib::spline1dcalc(power_spectrum, log10(k)));
			}
			double get_maxk(){
				return maxk;
			}
			double get_mink(){
				return mink;
			}
			double get_cooling_rate(double rho, double temp, double z);

	};

	struct Integrator{
		typedef double (*integrantFuncParam)(double x, void *params);
		typedef double (*integrantFunc)(double x);

		static double integrate_trapezoidal(integrantFunc func, double xmin, double xmax, int npoints);
		static double integrate_trapezoidal(integrantFuncParam func, void *params, double xmin, double xmax, int npoints);
	};

	struct NonLinearSolver{
		typedef double (*solveFunctionParam)(double x, void *params);
		typedef double (*solveFunction)(double x);

		static double solve_NonLinear_Bisect(solveFunctionParam func, void *params, double boundMin, double boundMax, double tol, int maxIter);
		static double solve_NonLinear_Bisect(solveFunction func, double boundMin, double boundMax, double tol, int maxIter);
		static double solve_NonLinear_Secant(solveFunctionParam func, void *params, double boundMin, double boundMax, double tol, int maxIter);
		static double solve_NonLinear_Secant(solveFunction func, double boundMin, double boundMax, double tol, int maxIter);
		static double solve_NonLinear_Brent(solveFunctionParam func, void *params, double boundMin, double boundMax, double tol, int maxIter);
		static double solve_NonLinear_Brent(solveFunction func, double boundMin, double boundMax, double tol, int maxIter);
		static double first_deriv(solveFunctionParam func, void *params, double Xeval, double step);
		static double first_deriv(solveFunction func, double Xeval, double step);
		static double second_deriv(solveFunctionParam func, void *params, double Xeval, double step);
		static double second_deriv(solveFunction func, double Xeval, double step);

		static std::vector<double> find_roots(solveFunctionParam func, void *params, double boundMin, double boundMax, double tol, int maxIter, int npoints);
		static std::vector<double> find_roots(solveFunction func, double boundMin, double boundMax, double tol, int maxIter, int npoints);

	};

	struct ParallelMapping{
		template <typename ReturnType, typename Mapable, typename ...Args>
		static std::vector<ReturnType> map_parallel(ReturnType(*func)(Mapable, Args...), int nThreads, std::vector<Mapable> *inputs, Args... args);
		
	};
	
}



#endif