#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "integration.h"
#include "interpolation.h"
#include "helpers.hpp"
#include "model.hpp"



namespace model{
	double Cosmology::E(double z){
		return sqrt(omegaR*pow(1.0+z, 4)+omegaM*pow(1.0+z, 3)+omegaL+(1.0-omegaM-omegaL-omegaR)*pow(1.0+z, 2));
	}
	double Cosmology::t(double z){
		alglib::autogkstate gkState;
		alglib::autogkreport gkRep;

		alglib::autogksmooth(0.0, 1.0/(1.0+z), gkState);
		double integral;
		alglib::autogkintegrate(gkState, t_integrant);
		autogkresults(gkState, integral, gkRep);

		return 1.0/(100*3.24e-20*h)*integral;
	}
	void Cosmology::t_integrant(double x, double xminusa, double bminusx, double &y, void *ptr){
		y=1/(x*E((1-x)/x));
	}

	double Cosmology::rho_crit_z(double z){
		return rho_crit_h2*pow(h*E(z), 2);
	}
	double Cosmology::D(double z){
		alglib::autogkstate gkState;
		alglib::autogkreport gkRep;
		alglib::autogksmooth(z/(1.0+z), 1.0, gkState);
		double integral;
		alglib::autogkintegrate(gkState, d_integrant);
		autogkresults(gkState, integral, gkRep);
		return 5*omegaM*E(z)/2*integral;
	}
	void Cosmology::d_integrant(double x, double xminusa, double bminusx, double &y, void *ptr){
		y=(1.0+(x/(1.0-x)))/pow(E(x/(1.0-x)),3)/pow(1.0-x, 2);
	}

	double Cosmology::T0_z(double z){
		return Cosmology::T0*(1+z);
	}

	double Cosmology::radius_of_mass(double m, double z){
		return pow(3*m/(4*M_PI*(rho_crit_h2*pow(h, 2)*omegaM*E(z)*pow(Mpc2cm,3)/M_sun)), 1.0/3.0);
	}
	double Cosmology::mass_of_radius(double r, double z){
		return (4*M_PI*(rho_crit_h2*pow(h, 2)*omegaM*E(z)*pow(Mpc2cm, 3)/M_sun))*pow(r, 3)/3;
	}

	double Cosmology::sigma_mass_sq(double m, double z, helpers::Interpolator *interp){
		double r=radius_of_mass(m, z);
		std::vector<void *> params{&r, interp};
		return 1.0/(2.0*pow(M_PI, 2))*helpers::Integrator::integrate_trapezoidal(&sigma_integrant, &params, log(interp->get_mink()), log(interp->get_maxk()), 2000);

	}
	double Cosmology::sigma_integrant(double log_k, void *ptr){
		std::vector<void *> params = *((std::vector<void *> *) ptr);
		helpers::Interpolator *interp=(helpers::Interpolator *) params[1];
		double r=*((double *) (params[0]));

		return pow(exp(log_k), 3)*interp->get_power_spectrum(exp(log_k))*pow(top_hat_window(exp(log_k)*r), 2);
	}

	double Cosmology::top_hat_window(double x){
		if (x<1e-4){
			return 1;
		}else{
			return 3*(sin(x)-x*cos(x))/pow(x,3);
		}
	}

	double Cosmology::salpeter_imf(double mass, double alpha, double norm){
		return norm*pow(mass, -alpha);
	}

	double Cosmology::integrated_imf(double mass, double alpha, double norm){
		return norm*pow(mass, -alpha+1)/(1-alpha);
	}

	double Cosmology::jeans_mass(double z){
		return 5.73e3*pow(Cosmology::omegaM*pow(Cosmology::h, 2)/0.15,-1.0/2.0)*pow(Cosmology::omegaB*pow(Cosmology::h, 2)/0.022,-3.0/5.0)*pow((1+z)/10.0, 3.0/2.0);
	}



	/*
	double model::ModelParams::star_formation_timescale(double v200){
		return t0_star*pow(v200/v0_star, -alpha_star);
	}

	double model::ModelParams::feedback_ejection_fraction(double v200){
		return pow(v200/v_hot, -alpha_hot);
	}*/
}

