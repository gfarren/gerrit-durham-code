#ifndef Model_H
#define Model_H

namespace model{
	struct Cosmology{

		static constexpr double rho_crit_h2=1.86e-29;
		static constexpr double omegaM=0.321;
		static constexpr double omegaR=9.28656e-5;
		static constexpr double omegaB=0.04897;
		static constexpr double omegaL=1-omegaR-omegaM;
		static constexpr double sigma8=0.81;
		static constexpr double h=0.6766;
		static constexpr double n=0.966;
		static constexpr double delta_crit=1.686;
		static constexpr double z_reion=6.5;
		static constexpr double T0=2.73;

		static double E(double z);
		static double t(double z);
		static double rho_crit_z(double z);
		static double D(double z);
		static double T0_z(double z);

		static double radius_of_mass(double m, double z);
		static double radius_of_mass(double m){
			return radius_of_mass(m, 0.0);
		}
		static double mass_of_radius(double r, double z);
		static double mass_of_radius(double r){
			return mass_of_radius(r, 0.0);
		}

		static double sigma_mass_sq(double m, double z, helpers::Interpolator *interp);
		static double sigma_mass_sq(double m, helpers::Interpolator *interp){
			return sigma_mass_sq(m, 0.0, interp);
		}

		static double salpeter_imf(double mass, double alpha, double norm=1.0);
		static double integrated_imf(double mass, double alpha, double norm=1.0);

		static double jeans_mass(double z);

		private:
			static double top_hat_window(double x);
			static void t_integrant(double x, double xminusa, double bminusx, double &y, void *ptr);
			static void d_integrant(double x, double xminusa, double bminusx, double &y, void *ptr);
			static double sigma_integrant(double x, void *ptr);

	};

	struct ModelParams{
		double mean_concentration;
		double mean_orbital_parameter_log10;
		double sd_orbital_parameter_log10;
		double f_df;
		double t_gas_post_reion;
		double fraction_t;
		double star_formation_ratio, feedback_coupling, imf_slope, sn_energy;
		double min_stellar_mass, max_stellar_mass, min_sn_mass;

		ModelParams(){
			mean_concentration=10.0;
			mean_orbital_parameter_log10=-0.14;
			sd_orbital_parameter_log10=0.26;
			f_df=1.0;
			t_gas_post_reion=2.0e4;
			fraction_t=1.0/100.0;
			star_formation_ratio=1.0;
			feedback_coupling=1.0;
			imf_slope=2.35;
			sn_energy=1.0e51;
			min_stellar_mass=0.1;
			max_stellar_mass=100.0;
			min_sn_mass=8.0;
			/*
			alpha_star=1.5;
			t0_star=1.0e6*365*24*3600; //1M years in seconds
			v0_star=300*1000*100;
			alpha_hot=5.5;
			v_hot=140*1000*100; // cm/s*/
		}

		//double star_formation_timescale(double v200);
		//double feedback_ejection_fraction(double v200);
	};

	static constexpr double Mpc2cm = 3.086e24; // cm/Mpc
	static constexpr double M_sun = 1.988e33; // g
	static constexpr double M_proton = 1.6726219e-24; // g
	static constexpr double G = 6.674e-8; // cm^3/(g s^2)
	static constexpr double Kb = 1.38e-16; // erg/K
}

#endif