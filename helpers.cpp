#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "interpolation.h"
#include <boost/math/special_functions/erf.hpp>
#include <fstream>
#include <sstream>
#include <iostream>
#include <thread>
#include <future>
#include <string>
#include <iterator>
#include <random>
#include <algorithm>
#include <typeinfo>
#include "H5Cpp.h"
#include "helpers.hpp"
#include "model.hpp"
#include "haloes.hpp"
#include "galaxies.hpp"

using namespace H5;

namespace helpers{
	Interpolator::Interpolator(double minZ, double maxZ, int stepsZ){
		set_growth(minZ, maxZ, stepsZ);
		set_time(minZ, maxZ, stepsZ);

	}

	Interpolator::Interpolator(double minM, double maxM, int stepsM, double minZ, double maxZ, int stepsZ, std::string power_spectrum_file, std::string cooling_table_file, bool molecules){
		set_growth(minZ, maxZ, stepsZ);
		set_time(minZ, maxZ, stepsZ);
		set_power_spectrum(power_spectrum_file);
		set_sigma_sq(minM, maxM, stepsM);
		set_cooling_table(cooling_table_file, molecules);
	}

	Interpolator::Interpolator(double minM, double maxM, int stepsM, double minZ, double maxZ, int stepsZ, std::string power_spectrum_file){
		set_growth(minZ, maxZ, stepsZ);
		set_time(minZ, maxZ, stepsZ);
		set_power_spectrum(power_spectrum_file);
		set_sigma_sq(minM, maxM, stepsM);
	}

	Interpolator::Interpolator(std::string power_spectrum_file, std::string cooling_table_file, bool molecules){
		set_growth(0.0, 60.0, 1000);
		set_time(0.0, 60.0, 1000);
		set_power_spectrum(power_spectrum_file);
		set_sigma_sq(1.0, 1.0e15, 1000);
		set_cooling_table(cooling_table_file, molecules);
	}

	Interpolator::Interpolator(std::string power_spectrum_file){
		set_growth(0.0, 60.0, 1000);
		set_time(0.0, 60.0, 1000);
		set_power_spectrum(power_spectrum_file);
		set_sigma_sq(1.0, 1.0e15, 1000);
	}


	Interpolator::Interpolator(){
		set_growth(0.0, 60.0, 1000);
		set_time(0.0, 60.0, 1000);
	}

	void Interpolator::set_sigma_sq(double minM, double maxM, int steps){
		std::vector<double> ms, sigma_sqs;
		double m, sig_sq;
		double stepSize=(log10(maxM)-log10(minM))/double(steps);
		alglib::real_1d_array m_vals, sigma_sq_vals;
		
		for(int i=0; i<=steps; i++){
			m=pow(10.0, log10(minM)+stepSize*double(i));
			sig_sq=model::Cosmology::sigma_mass_sq(m, 0.0, this);

			ms.push_back(log10(m));
			sigma_sqs.push_back(log10(sig_sq));
		}
		m_vals.setcontent(ms.size(), &(ms[0]));
		sigma_sq_vals.setcontent(sigma_sqs.size(), &(sigma_sqs[0]));

		alglib::spline1dbuildlinear(m_vals, sigma_sq_vals, sigma_sq);
		alglib::spline1dbuildlinear(sigma_sq_vals, m_vals, sigma_sq_inverse);
	}

	void Interpolator::set_power_spectrum(std::string filename){
		std::ifstream power_spectrum_file;
		std::vector<double> ks, ps;
		alglib::real_1d_array k_vals, p_vals;

		power_spectrum_file.open(filename);
		if(!power_spectrum_file){
			std::cerr << "Unable to open file "<<filename;
    		exit(1);   // call system to stop
		}

		std::vector<std::vector<double> > power_spectrum_file_content=Interpolator::read_file(&power_spectrum_file);
		for(int i=0; i<power_spectrum_file_content[0].size(); i++){
			ks.push_back(log10(power_spectrum_file_content[0][i]));
			ps.push_back(log10(power_spectrum_file_content[1][i]));
		}

		maxk = pow(10.0, *max_element(ks.begin(), ks.end()));
		mink = pow(10.0, *min_element(ks.begin(), ks.end()));

		k_vals.setcontent(ks.size(), &(ks[0]));
		p_vals.setcontent(ps.size(), &(ps[0]));

		alglib::spline1dbuildlinear(k_vals, p_vals, power_spectrum);

	}

	void Interpolator::set_growth(double minZ, double maxZ, int steps){
		std::vector<double> zs, ds;
		double z, d;
		double stepSize=(maxZ-minZ)/double(steps);
		alglib::real_1d_array z_vals, d_vals;

		double d0=model::Cosmology::D(0.0);

		for(int i=0; i<=steps; i++){
			z=minZ+double(i)*stepSize;
			d=model::Cosmology::D(z);

			zs.push_back(z);
			ds.push_back(log10(d/d0));
		}

		d_vals.setcontent(ds.size(), &(ds[0]));
		z_vals.setcontent(zs.size(), &(zs[0]));

		alglib::spline1dbuildlinear(z_vals, d_vals, d_of_z_interp);
		alglib::spline1dbuildlinear(d_vals, z_vals, z_of_d_interp);
	}

	void Interpolator::set_time(double minZ, double maxZ, int steps){
		std::vector<double> zs, ts;
		double z, t;
		double stepSize=(maxZ-minZ)/double(steps);
		alglib::real_1d_array z_vals, t_vals;

		for(int i=0; i<=steps; i++){
			z=minZ+double(i)*stepSize;
			t=model::Cosmology::t(z);

			zs.push_back(z);
			ts.push_back(log10(t));
		}

		t_vals.setcontent(ts.size(), &(ts[0]));
		z_vals.setcontent(zs.size(), &(zs[0]));

		alglib::spline1dbuildlinear(z_vals, t_vals, t_of_z_interp);
		alglib::spline1dbuildlinear(t_vals, z_vals, z_of_t_interp);
	}

	void Interpolator::set_cooling_table(std::string filename, bool molecules){
		H5File fp(filename.c_str(),H5F_ACC_RDONLY);

		DataSet cooling=fp.openDataSet("Tdep/Cooling");
		DataSet rhoBins=fp.openDataSet("TableBins/DensityBins");
		DataSet tempBins=fp.openDataSet("TableBins/TemperatureBins");
		DataSet zBins=fp.openDataSet("TableBins/RedshiftBins");

		hsize_t dims_out[cooling.getSpace().getSimpleExtentNdims()];
      	int rank = cooling.getSpace().getSimpleExtentDims( dims_out, NULL);

      	int size_array=1;
      	for (int j=0; j<rank; j++){
      		size_array*=dims_out[j];
      	}
      	
      	double *data = new double[size_array];
      	//double *data_select= new double[size_array/dims_out[2]/dims_out[4]];
      	cooling.read(data, PredType::NATIVE_DOUBLE);

      	/*
      	for(int z=0; z<dims_out[0]; z++){
      		for(int t=0; t<dims_out[1]; t++){
      			for(int r=0; r<dims_out[3]; r++){
      				data_select[(z*dims_out[2]+t)*dims_out[3]+r] = data[((z*dims_out[1]+t)*dims_out[2]*dims_out[3]+r)*dims_out[4]+20];
      			}
      		}
      	}*/

      	double *zBinsArray = new double[dims_out[0]];
      	zBins.read(zBinsArray, PredType::NATIVE_DOUBLE);
      	this->zBins_cooling=std::vector<double> (zBinsArray, zBinsArray + dims_out[0]);
      	double *tempBinsArray = new double[dims_out[1]];
      	tempBins.read(tempBinsArray, PredType::NATIVE_DOUBLE); 
      	double *rhoBinsArray = new double[dims_out[3]];
      	rhoBins.read(rhoBinsArray, PredType::NATIVE_DOUBLE);

      	double *interpolationData = new double[dims_out[1]*dims_out[3]];
      	for(int z=0; z<dims_out[0]; z++){
      		for(int t=0; t<dims_out[1]; t++){
      			for(int r=0; r<dims_out[3]; r++){
      				if(molecules){
      					interpolationData[t*dims_out[3]+r] = data[((z*dims_out[1]+t)*dims_out[2]*dims_out[3]+r)*dims_out[4]+20];
      				}else{
      					interpolationData[t*dims_out[3]+r] = log10(pow(10, data[((z*dims_out[1]+t)*dims_out[2]*dims_out[3]+r)*dims_out[4]+20])-pow(10, data[((z*dims_out[1]+t)*dims_out[2]*dims_out[3]+r)*dims_out[4]+12]));
      				}
      			}
      		}
      		alglib::spline2dinterpolant s;
      		alglib::real_1d_array rho;
		    alglib::real_1d_array temp;
		    alglib::real_1d_array vals;
		    rho.setcontent(dims_out[3], rhoBinsArray);
		    temp.setcontent(dims_out[1], tempBinsArray);
		    vals.setcontent(dims_out[1]*dims_out[3], interpolationData);
      		alglib::spline2dbuildbicubicv(rho, dims_out[3], temp, dims_out[1], vals, 1, s);

      		cooling_tables.push_back(s);
      	}

	}

	double Interpolator::get_cooling_rate(double rho, double temp, double z){
		if (z>10.0){
			return pow(10.0, alglib::spline2dcalc(cooling_tables[zBins_cooling.size()-1], log10(rho), log10(temp)));
		}
		for(int i=0; i<zBins_cooling.size()-1; i++){
			if (abs(z-zBins_cooling[i])<abs(z-zBins_cooling[i+1])){
				return pow(10.0, alglib::spline2dcalc(cooling_tables[i], log10(rho), log10(temp)));
			}
		}

	}



	std::vector<std::vector<double> > Interpolator::read_file(std::ifstream *file){
		std::stringstream buffer;
		std::vector<double> out_1,out_2;

		buffer << file->rdbuf();
		std::string vals_one = buffer.str().substr(0, buffer.str().find("\n"));
		std::string vals_two=buffer.str().substr(buffer.str().find("\n")+1, buffer.str().length());

		out_1=Interpolator::convert2vec(vals_one);
		out_2=Interpolator::convert2vec(vals_two);

		std::vector<std::vector<double> > out;
		out.push_back(out_1);
		out.push_back(out_2);
		return out;
	}

	std::vector<double> Interpolator::convert2vec(std::string str){
		std::vector<double> vec;
		std::istringstream s(str);
		std::copy(
			std::istream_iterator<double>(s)  // start
			, std::istream_iterator<double>()  // end
			, std::back_inserter(vec)  // destination
	  	);
	  	return vec;
	}


	double Integrator::integrate_trapezoidal(integrantFunc func, double xmin, double xmax, int npoints){
		double stepSize=(xmax-xmin)/double(npoints);
		double sum=0.0;
		for(int i=0; i<npoints; i++){
			sum+= func(xmin+stepSize*double(i));
		}
		return (func(xmin)+func(xmax)+2*sum)*(xmax-xmin)/(2*npoints);

	}

	double Integrator::integrate_trapezoidal(integrantFuncParam func, void *params, double xmin, double xmax, int npoints){
		double stepSize=(xmax-xmin)/double(npoints);
		double sum=0.0;
		for(int i=0; i<npoints; i++){
			sum+= func(xmin+stepSize*double(i), params);
		}
		return (func(xmin, params)+func(xmax, params)+2*sum)*(xmax-xmin)/(2*npoints);
	}

	double NonLinearSolver::solve_NonLinear_Bisect(solveFunctionParam func, void *params, double boundMin, double boundMax, double tol, int maxIter){
		double val1,val2,val3;
		int counter=0;
		val1=func(boundMin, params);
		val2=func(boundMax, params);
		do{
			if (val1*val2>0.0){
				throw "Bisection failed! Bounds invalid!";
			}else{
				val3=func((boundMin+boundMax)/2.0, params);
				if(val3>0.0){
					boundMax=(boundMin+boundMax)/2.0;
				}else if (val3<0.0){
					boundMin=(boundMin+boundMax)/2.0;
				}else{
					return (boundMin+boundMax)/2.0;
				}
			}
			counter++;
		}while(abs(2.0*(boundMin-boundMax)/(boundMin+boundMax))>tol && counter<maxIter);
		if(counter==maxIter){
			std::cout<<"Bisection Solver did not converge! After "<<maxIter<<" iterations a tolerance of "<<abs(2.0*(boundMin-boundMax)/(boundMin+boundMax))<<" was reached."<<std::endl;
		}
		return (boundMin+boundMax)/2.0;
	}
	double NonLinearSolver::solve_NonLinear_Bisect(solveFunction func, double boundMin, double boundMax, double tol, int maxIter){
		double val1,val2,val3;
		int counter=0;
		val1=func(boundMin);
		val2=func(boundMax);
		do{
			if (val1*val2>0.0){
				throw "Bisection failed! Bounds invalid!";
			}else{
				val3=func((boundMin+boundMax)/2.0);
				if(val3>0.0){
					boundMax=(boundMin+boundMax)/2.0;
				}else if (val3<0.0){
					boundMin=(boundMin+boundMax)/2.0;
				}else{
					return (boundMin+boundMax)/2.0;
				}
			}
			counter++;
		}while(abs(2.0*(boundMin-boundMax)/(boundMin+boundMax))>tol && counter<maxIter);
		if(counter==maxIter){
			std::cout<<"Bisection Solver did not converge! After "<<maxIter<<" iterations a tolerance of "<<abs(2.0*(boundMin-boundMax)/(boundMin+boundMax))<<" was reached."<<std::endl;
		}
		return (boundMin+boundMax)/2.0;

	}
	double NonLinearSolver::solve_NonLinear_Secant(solveFunctionParam func, void *params, double boundMin, double boundMax, double tol, int maxIter){
		double val1, val2, val_tmp, x1, x2, x_tmp;
		int counter=0;
		x1=boundMin;
		x2=boundMax;

		val1=func(x1, params);
		val2=func(x2, params);
		do{
			x_tmp=x2-val2*(x2-x1)/(val2-val1); //first_deriv(func, params, x, abs(x-x_old)/10.0);
			x1=x2;
			x2=x_tmp;

			if(x2<boundMin){
				x2=boundMin;
			}else if(x2>boundMax){
				x2=boundMax;
			}
			val1=val2;
			val2=func(x2, params);
			counter++;
		}while(abs(x1-x2)>tol && counter<maxIter);
		if(counter==maxIter){
			std::cout<<"Secant Solver did not converge! After "<<maxIter<<" iterations a tolerance of "<<abs(x1-x2)<<" was reached."<<std::endl;
		}
		return x2;
	}
	double NonLinearSolver::solve_NonLinear_Secant(solveFunction func, double boundMin, double boundMax, double tol, int maxIter){
		double val1, val2, val_tmp, x1, x2, x_tmp;
		int counter=0;
		x1=boundMin;
		x2=boundMax;

		val1=func(x1);
		val2=func(x2);
		do{
			x_tmp=x2-val2*(x2-x1)/(val2-val1); //first_deriv(func, params, x, abs(x-x_old)/10.0);
			x1=x2;
			x2=x_tmp;
			
			if(x2<boundMin){
				x2=boundMin;
			}else if(x2>boundMax){
				x2=boundMax;
			}
			val1=val2;
			val2=func(x2);
			counter++;
		}while(abs(x1-x2)>tol && counter<maxIter);
		if(counter==maxIter){
			std::cout<<"Secant Solver did not converge! After "<<maxIter<<" iterations a tolerance of "<<abs(x1-x2)<<" was reached."<<std::endl;
		}
		return x2;
	}

	std::vector<double> NonLinearSolver::find_roots(solveFunctionParam func, void *params, double boundMin, double boundMax, double tol, int maxIter, int npoints){
		std::vector<double> roots=std::vector<double>();
		double stepSize=(boundMax-boundMin)/npoints;
		double x_last=boundMin;
		double val_last=func(boundMin, params);
		double val;
		double r;
		for(int i=1; i<=npoints; i++){
			val=func(x_last+stepSize, params);
			if(val*val_last<0){
				r=solve_NonLinear_Secant(func, params, x_last, x_last+stepSize, tol, maxIter);
				roots.push_back(r);
			}
			val_last=val;
			x_last+=stepSize;
		}
		return roots;
	}

	std::vector<double> NonLinearSolver::find_roots(solveFunction func, double boundMin, double boundMax, double tol, int maxIter, int npoints){
		std::vector<double> roots=std::vector<double>();
		double stepSize=(boundMax-boundMin)/npoints;
		double x_last=boundMin;
		double val_last=func(boundMin);
		double val;
		double r;
		for(int i=1; i<=npoints; i++){
			val=func(x_last+stepSize);
			if(val*val_last<0){
				r=solve_NonLinear_Secant(func, x_last, x_last+stepSize, tol, maxIter);
				roots.push_back(r);
			}
			val_last=val;
			x_last+=stepSize;
		}
		return roots;
	}


	double NonLinearSolver::first_deriv(solveFunctionParam func, void *params, double Xeval, double step){
		return (func(Xeval+step/2.0, params)-func(Xeval-step/2.0, params))/(step);
	}
	double NonLinearSolver::first_deriv(solveFunction func, double Xeval, double step){
		return (func(Xeval+step/2.0)-func(Xeval-step/2))/(step);
	}
	double NonLinearSolver::second_deriv(solveFunctionParam func, void *params, double Xeval, double step){
		return (func(Xeval+step/2.0, params)-2.0*func(Xeval, params)+func(Xeval-step/2.0, params))/pow(step/2.0, 2);
	}
	double NonLinearSolver::second_deriv(solveFunction func, double Xeval, double step){
		return (func(Xeval+step/2.0)-2.0*func(Xeval)+func(Xeval-step/2.0))/pow(step/2.0, 2);
	}

	static inline void native_cpuid(unsigned int *eax, unsigned int *ebx, unsigned int *ecx, unsigned int *edx)
	{
	    /* ecx is often an input as well as an output. */
	    asm volatile("cpuid"
	        : "=a" (*eax),
	        "=b" (*ebx),
	        "=c" (*ecx),
	        "=d" (*edx)
	        : "0" (*eax), "2" (*ecx)
	        : "memory");
	}

	template <typename ReturnType, typename Mapable, typename ...Args>
	std::vector<ReturnType> ParallelMapping::map_parallel(ReturnType(*func)(Mapable, Args...), int nThreads, std::vector<Mapable> *inputs, Args... args){
		
		nThreads=std::min(nThreads, (int)inputs->size());

		int lenChunks[nThreads];
		int posFirst[nThreads];
		for(int i=0; i<inputs->size(); i++){
			if(i<nThreads){
				lenChunks[i%nThreads]=1;
			}else{
				lenChunks[i%nThreads]++;
			}
		}
		posFirst[0]=0;
		for(int i=1; i<nThreads; i++){
			posFirst[i]=posFirst[i-1]+lenChunks[i-1];
		} 

		auto executor = [](std::promise<std::vector<ReturnType>> *promiseObj ,ReturnType(*func)(Mapable, Args...), std::vector<Mapable> *input, Args... args){
			std::vector<ReturnType> output;
			for(int i=0; i<input->size(); i++){
				output.push_back(func(input->at(i), args...));
			}
			promiseObj->set_value(output);
		};

		std::future<std::vector<ReturnType>> futures[nThreads];
		std::promise<std::vector<ReturnType>> promises[nThreads];
		std::thread threads[nThreads];

		for (int i=0; i<nThreads; i++){
			futures[i]=promises[i].get_future();
			threads[i]=std::thread(executor, &promises[i], *func, new std::vector<Mapable>(inputs->begin()+posFirst[i], inputs->begin()+posFirst[i]+lenChunks[i]), args...);
		}
		std::vector<ReturnType> output;
		std::vector<ReturnType> tmp;
		for(int i=0; i<nThreads; i++){
			tmp=futures[i].get();
			threads[i].join();
			output.insert(end(output), begin(tmp), end(tmp));
		}
		std::cout<<output.size()<<std::endl;
		return output;
		
	}

	//template void ParallelMapping::map_parallel<double, double, std::string, int>(double (*func)(double, std::string, int), int nThreads, std::vector<double> *inputs, std::vector<double> *output_final, std::string arg1, int arg2);
	//template to allow parallel halo sample generation
	template std::vector<std::vector<haloes::Halo *>*> ParallelMapping::map_parallel<std::vector<haloes::Halo *>*, double, double, int, double, bool, helpers::Interpolator*>(std::vector<haloes::Halo *>* (*func)(double, double, int, double, bool, helpers::Interpolator*), int nThreads, std::vector<double> *inputs, double z0, int n, double deltaW, bool populateChildren, helpers::Interpolator *interp);
	template std::vector<std::vector<haloes::Halo *>*> ParallelMapping::map_parallel<std::vector<haloes::Halo *>*, double, double, int, double, double, double, bool, helpers::Interpolator*>(std::vector<haloes::Halo *>* (*func)(double, double, int, double, double, double, bool, helpers::Interpolator*), int nThreads, std::vector<double> *inputs, double z0, int n, double minMass, double z_max, double deltaW, bool populateChildren, helpers::Interpolator *interp);
	//template to allow parallel computing of cooled fractions
	template std::vector<double> ParallelMapping::map_parallel<double, std::vector<haloes::Halo *>*, helpers::Interpolator*, model::ModelParams*>(double(*func)(std::vector<haloes::Halo *>*,helpers::Interpolator*, model::ModelParams*), int nThreads, std::vector<std::vector<haloes::Halo *>*>* halo_samples, helpers::Interpolator *interp, model::ModelParams *params);
		
	//template to make a single sample of haloes in parallel
	template std::vector<haloes::Halo*> ParallelMapping::map_parallel<haloes::Halo*, int, double, double, double, double, double, bool, helpers::Interpolator*>(haloes::Halo*(*func)(int, double, double, double, double, double, bool, Interpolator*), int nThreads, std::vector<int> *ids, double mass, double z0, double minMass, double z_max, double deltaW, bool populateChildren, Interpolator *interp);
	template std::vector<haloes::Halo*> ParallelMapping::map_parallel<haloes::Halo*, int, double, double, double, bool, helpers::Interpolator*>(haloes::Halo*(*func)(int, double, double, double, bool, Interpolator*), int nThreads, std::vector<int> *ids, double mass, double z0, double deltaW, bool populateChildren, Interpolator *interp);

	//template to allow parallel computation of satellite population
	template std::vector<galaxies::Galaxy*> ParallelMapping::map_parallel<galaxies::Galaxy*, haloes::Halo*, helpers::Interpolator*, model::ModelParams*>(galaxies::Galaxy*(*func)(haloes::Halo*, Interpolator*, model::ModelParams*), int nThreads, std::vector<haloes::Halo*> *haloes, Interpolator *interp, model::ModelParams *params);



}