#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "interpolation.h"
#include "integration.h"
#include <boost/math/special_functions/erf.hpp>
#include <fstream>
#include <sstream>
#include <string>
#include <iterator>
#include <random>
#include <algorithm>
#include "helpers.hpp"
#include "model.hpp"
#include "haloes.hpp"
#include "galaxies.hpp"

namespace galaxies{

	Galaxy::Galaxy(model::ModelParams *params, helpers::Interpolator *interp){
		orbital_parameter_distribution = std::normal_distribution<double> (params->mean_orbital_parameter_log10, params->sd_orbital_parameter_log10);
		this->interp=interp;
		this->params=params;
		this->merged_dm_mass=0.0;
		this->cold_gas=0.0;
		this->hot_gas=0.0;
		this->r_cool=0.0;
		this->stellar_mass=0.0;
		this->e_radiated=0.0;
	}

	void Galaxy::merge_satellites(double z){
		std::vector<Galaxy *> not_merged;
		for(int i=0; i<satellites.size(); i++){
			if(satellites[i]->get_z_will_merge() > z){
				stellar_mass+=satellites[i]->get_stellar_mass();
				this->add_cold_gas(satellites[i]);

				
				merged_galaxies.push_back(satellites[i]);
				merged_dm_mass+=satellites[i]->get_merged_dm_mass();
				satellites[i]->set_is_merged(z);
			}else{
				not_merged.push_back(satellites[i]);
			}
		}
		satellites=not_merged;
	}

	void Galaxy::becomes_satellite(haloes::Halo *parent_halo){
		if (!is_satellite && !is_merged){
			is_satellite=true;
			z_became_satellite=parent_halo->get_z();
		}

		double t_dyn=haloes::Halo::get_R200(parent_halo)/haloes::Halo::get_V200(parent_halo);
		double orbital_parameter=pow(10.0, orbital_parameter_distribution(generator));

		double t_mrg=params->f_df*orbital_parameter*t_dyn*0.3722/log(parent_halo->get_mass()/merged_dm_mass)*parent_halo->get_mass()/merged_dm_mass;
		
		z_will_merge=std::max(interp->z_of_t(interp->t_of_z(parent_halo->get_z())+t_mrg), 0.0);
	}

	void Galaxy::populate_galaxies(haloes::Halo *halo_in){
		this->halo_history.push_back(halo_in);												//put current halo in the history of this galaxy

		if (halo_in->get_progenitors().size() > 1){											//if the current halo has more than one progenitor
			this->populate_galaxies(halo_in->get_most_massive_progenitor());				//assume that the current central galaxy orriginated in the most massive progenitor
			this->merge_satellites(halo_in->get_z());										//merge the satellites of the most massive progenitor into the central galaxy based on dynaical friction timescale
			this->merged_dm_mass+=halo_in->get_smooth_mass_growth();						//account for mass accreated below the resolution limit


			for(int i=0; i<halo_in->get_progenitors().size(); i++){							
				if(halo_in->get_progenitors()[i] != halo_in->get_most_massive_progenitor()){
					Galaxy *g = new Galaxy(params, interp);									//create new galaxies in each of the other progenitors
					g->populate_galaxies(halo_in->get_progenitors()[i]);					//populate all those galaxies (recursive)
					g->merge_satellites(halo_in->get_z());									//for each non-central galaxy (central galaxies of progenitor haloes) check which of the existing satellites would have merged with that galaxy by the present time

					for (int j=0; j<g->get_satellites().size(); j++){						//loop over surviving satellites

						g->get_satellites()[j]->becomes_satellite(halo_in);					//each surviving satellite becomes a satellite of the current central galaxy (new random orbital parameters)
						this->satellites.push_back(g->get_satellites()[j]);	
					}
					g->becomes_satellite(halo_in);											//the central galaxy of the progenitor halo also becomes a satellite
					this->satellites.push_back(g);
				}
			}
			//std::cout<<"____"<<std::endl;
		}else if(halo_in->get_progenitors().size() == 1){									//if there is only one progenitor we just assume that the current galaxy came from that progenitor
			this->populate_galaxies(halo_in->get_most_massive_progenitor());
			this->merge_satellites(halo_in->get_z());
			this->merged_dm_mass+=halo_in->get_smooth_mass_growth();

		}else{
			this-> merged_dm_mass=halo_in->get_mass();										//if there is no progenitor assume simply that all the dm in the halo is smooth
		}
		this->merged_dm_mass_history.push_back(merged_dm_mass);
		
	}

	void Galaxy::populate_galaxies_wGas(haloes::Halo *halo_in){
		this->halo_history.push_back(halo_in);												//put current halo in the history of this galaxy
		
		if (halo_in->get_progenitors().size() > 1){											//if the current halo has more than one progenitor
			this->populate_galaxies_wGas(halo_in->get_most_massive_progenitor());			//assume that the current central galaxy orriginated in the most massive progenitor

			this->merge_satellites(halo_in->get_z());										//merge the satellites of the most massive progenitor into the central galaxy based on dynamical friction timescale
			this->merged_dm_mass+=halo_in->get_smooth_mass_growth();						//account for mass accreated below the resolution limit
			this->smoothly_accrete_gas(halo_in->get_smooth_mass_growth());						


			for(int i=0; i<halo_in->get_progenitors().size(); i++){							
				if(halo_in->get_progenitors()[i] != halo_in->get_most_massive_progenitor()){
					std::cout<<i<<std::endl;
					Galaxy *g = new Galaxy(params, interp);									//create new galaxies in each of the other progenitors
					g->populate_galaxies_wGas(halo_in->get_progenitors()[i]);					//populate all those galaxies (recursive)
					g->merge_satellites(halo_in->get_z());									//for each non-central galaxy (central galaxies of progenitor haloes) check which of the existing satellites would have merged with that galaxy by the present time

					for (int j=0; j<g->get_satellites().size(); j++){						//loop over surviving satellites

						g->get_satellites()[j]->becomes_satellite(halo_in);					//each surviving satellite becomes a satellite of the current central galaxy (new random orbital parameters)
						this->satellites.push_back(g->get_satellites()[j]);	
					}
					g->becomes_satellite(halo_in);											//the central galaxy of the progenitor halo also becomes a satellite
					add_hot_gas(g);
					add_energy_radiated(g);
					this->satellites.push_back(g);
				}
			}
			//std::cout<<"____"<<std::endl;
		}else if(halo_in->get_progenitors().size() == 1){									//if there is only one progenitor we just assume that the current galaxy came from that progenitor
			this->populate_galaxies_wGas(halo_in->get_most_massive_progenitor());

			this->merge_satellites(halo_in->get_z());
			this->merged_dm_mass+=halo_in->get_smooth_mass_growth();
			this->smoothly_accrete_gas(halo_in->get_smooth_mass_growth());

		}else{
			this-> merged_dm_mass=halo_in->get_mass();										//if there is no progenitor assume simply that all the dm in the halo is smooth
			this->intial_gas(halo_in->get_mass());
		}

		if(halo_in->get_parent() != NULL){
			std::cout<<hot_gas<<"; "<<cold_gas<<"; "<<r_cool<<std::endl;
			this->cool_gas(halo_in->get_z(), halo_in->get_parent()->get_z(), halo_in, true, false);
			std::cout<<hot_gas<<"; "<<cold_gas<<"; "<<r_cool<<std::endl;
			this->merged_dm_mass_history.push_back(merged_dm_mass);
			this->cold_gas_history.push_back(cold_gas);
			this->hot_gas_history.push_back(hot_gas);
			this->r_cool_history.push_back(r_cool);
			this->e_radiated_history.push_back(e_radiated);
		}

		std::cout<<(get_cold_gas()+get_hot_gas())/halo_in->get_mass()<<std::endl;
		
	}

	void Galaxy::populate_galaxies_wStars(haloes::Halo *halo_in){
		this->halo_history.push_back(halo_in);												//put current halo in the history of this galaxy
		
		if (halo_in->get_progenitors().size() > 1){											//if the current halo has more than one progenitor
			this->populate_galaxies_wStars(halo_in->get_most_massive_progenitor());			//assume that the current central galaxy orriginated in the most massive progenitor

			this->merge_satellites(halo_in->get_z());										//merge the satellites of the most massive progenitor into the central galaxy based on dynamical friction timescale
			this->merged_dm_mass+=halo_in->get_smooth_mass_growth();						//account for mass accreated below the resolution limit
			this->smoothly_accrete_gas(halo_in->get_smooth_mass_growth());						


			for(int i=0; i<halo_in->get_progenitors().size(); i++){							
				if(halo_in->get_progenitors()[i] != halo_in->get_most_massive_progenitor()){
					Galaxy *g = new Galaxy(params, interp);									//create new galaxies in each of the other progenitors
					g->populate_galaxies_wStars(halo_in->get_progenitors()[i]);				//populate all those galaxies (recursive)
					g->merge_satellites(halo_in->get_z());									//for each non-central galaxy (central galaxies of progenitor haloes) check which of the existing satellites would have merged with that galaxy by the present time

					for (int j=0; j<g->get_satellites().size(); j++){						//loop over surviving satellites

						g->get_satellites()[j]->becomes_satellite(halo_in);					//each surviving satellite becomes a satellite of the current central galaxy (new random orbital parameters)
						this->satellites.push_back(g->get_satellites()[j]);	
					}
					g->becomes_satellite(halo_in);											//the central galaxy of the progenitor halo also becomes a satellite
					add_hot_gas(g);
					add_energy_radiated(g);
					this->satellites.push_back(g);
				}
			}
			//std::cout<<"____"<<std::endl;
		}else if(halo_in->get_progenitors().size() == 1){									//if there is only one progenitor we just assume that the current galaxy came from that progenitor
			this->populate_galaxies_wStars(halo_in->get_most_massive_progenitor());

			this->merge_satellites(halo_in->get_z());
			this->merged_dm_mass+=halo_in->get_smooth_mass_growth();
			this->smoothly_accrete_gas(halo_in->get_smooth_mass_growth());

		}else{
			this-> merged_dm_mass=halo_in->get_mass();										//if there is no progenitor assume simply that all the dm in the halo is smooth
			this->intial_gas(halo_in->get_mass());
		}

		if(halo_in->get_parent() != NULL){
			std::cout<<hot_gas<<"; "<<cold_gas<<"; "<<stellar_mass<<std::endl;
			this->cool_gas(halo_in->get_z(), halo_in->get_parent()->get_z(), halo_in, true, true);
			std::cout<<hot_gas<<"; "<<cold_gas<<"; "<<stellar_mass<<std::endl;
			this->merged_dm_mass_history.push_back(merged_dm_mass);
			this->cold_gas_history.push_back(cold_gas);
			this->hot_gas_history.push_back(hot_gas);
			this->r_cool_history.push_back(r_cool);
			this->e_radiated_history.push_back(e_radiated);
		}

		std::cout<<(get_cold_gas()+get_hot_gas()+get_stellar_mass()+get_ejected_gas())/halo_in->get_mass()<<std::endl;
		
	}

	double Galaxy::t_cool(double t_gas, double rho_b_tilde, double z, helpers::Interpolator *interp){
		double rho_hot=model::Cosmology::rho_crit_z(z)*model::Cosmology::omegaB*rho_b_tilde;
		return 3*model::Kb/2*t_gas/(interp->get_cooling_rate(rho_hot/model::M_proton, t_gas, z)*rho_hot/model::M_proton);
	}
  	double Galaxy::cool_luminosity(double rmin, double rmax, double m200, double rho_norm, double concentration, double t_gas, double z, helpers::Interpolator *interp){
		
		double r200 = haloes::Halo::get_R200(m200, z);
		double t200 = haloes::Halo::get_T200(m200, z);

		std::vector<void *> params{&t_gas, &z, &rho_norm, &concentration, &t200, interp};

		alglib::autogkstate gkState;
		alglib::autogkreport gkRep;
		alglib::autogksmooth(rmin, rmax, gkState);
		double integral;
		alglib::autogkintegrate(gkState, luminosity_integrant, &params);
		autogkresults(gkState, integral, gkRep);
		return 4.0*M_PI*pow(r200,3)*integral;
	}

  	void Galaxy::luminosity_integrant(double r_tilde, double xminusa, double bminusx, double &y, void *ptr){
  		std::vector<void *> param = *((std::vector<void *> *) ptr);
		double t_gas=*((double *) param[0]);
		double z=*((double *) param[1]);
		double rho_norm=*((double *) param[2]);
		double concentration = *((double *) param[3]);
  		double t200 = *((double *)param[4]);
		helpers::Interpolator *interp= (helpers::Interpolator *)param[5];
		//std::cout<<t_gas<<"; "<<z<<"; "<<rho_b_tilde(1.0)<<std::endl;

		double rho_hot=model::Cosmology::rho_crit_z(z)*model::Cosmology::omegaB*rho_norm*haloes::Halo::get_rhoB_tilde(r_tilde, concentration, t200/t_gas);
    	y=interp->get_cooling_rate(rho_hot/model::M_proton, t_gas, z)*pow(rho_hot/model::M_proton*r_tilde, 2);

  	}
  	double Galaxy::t_free_fall(double r0, double m_enclosed_tilde, double m200, double z){
  		double r200 = haloes::Halo::get_R200(m200, z);
  		double rho_mean = 3.0*m_enclosed_tilde*m200*model::M_sun/(4.0*M_PI*pow(r200*r0,3));
  		return sqrt(3.0*M_PI/(32.0*model::G*rho_mean));
  	}

  	double Galaxy::r_cool_solver(double log_r, void *params){
  		std::vector<void *> param = *((std::vector<void *> *) params);
  		double z=*((double *)param[0]);
  		double t_cool_avail=*((double *)param[1]);
  		double t_gas=*((double *)param[2]);
  		double rho_norm = *((double *) param[3]);
  		double concentration = *((double *) param[4]);
  		double t200 = *((double *)param[5]);
  		helpers::Interpolator *interp = (helpers::Interpolator *)param[6];

  		return galaxies::Galaxy::t_cool(t_gas, rho_norm*haloes::Halo::get_rhoB_tilde(exp(log_r), concentration, t200/t_gas), z, interp)-t_cool_avail;
  	}

  	void Galaxy::intial_gas(double dm_mass){
  		std::cout<<"Initialling gas content!"<<std::endl;
  		hot_gas=model::Cosmology::omegaB/model::Cosmology::omegaM*dm_mass;
  	}

  	void Galaxy::smoothly_accrete_gas(double smoothly_accreted_dm_mass){
  		hot_gas+=model::Cosmology::omegaB/model::Cosmology::omegaM*smoothly_accreted_dm_mass;
  	}

  	void Galaxy::cool_gas(double z0, double z_final, haloes::Halo *halo, bool adaptive, bool stars){
  		double t0=interp->t_of_z(z0);
  		double tf=interp->t_of_z(z_final);
  		double z,t,dt,t_cool_avail;
  		int counter=0;
  		double r_cool_tmp, r200;
  		double m_cold_new;
  		double t_gas, t200;
  		double concentration = halo->get_c();
  		double m200=halo->get_mass();
  		
  		double density_norm;
  		std::vector<double> roots;
  		
  		
  		t=t0;
  		z=z0;
  		r_cool_tmp=r_cool;
  		t200=haloes::Halo::get_T200(m200, z);

  		std::vector<void *> params{&z, &t_cool_avail, &t200, &density_norm, &concentration, &t200, interp};

  		std::cout<<"Starting gas cooling!"<<std::endl;
  		//std::cout<<z0<<"; "<<z_final<<"; "<<m200<<"; "<<t200<<std::endl;


		dt=(tf-t0)/1000;
		
  		while(t<tf){
			r200=haloes::Halo::get_R200(halo);
  			z=interp->z_of_t(t);

  			if(z>model::Cosmology::z_reion){
	  			t_gas=t200;
	  		}else{
	  			t_gas=std::max(this->params->t_gas_post_reion, t200);
	  		}

	  		density_norm=hot_gas/m200/(haloes::Halo::get_Mb_tilde(1.0, concentration,t200/t_gas)-haloes::Halo::get_Mb_tilde(r_cool/r200, concentration, t200/t_gas));
	  		
	  		//std::cout<<density_norm<<std::endl;
	  		if(adaptive && density_norm>0){
  				dt=std::max(t_cool(t_gas, density_norm*haloes::Halo::get_rhoB_tilde(std::max(r_cool/r200, 1.0e-12), concentration, t200/t_gas), z, interp)-t_cool_avail, t_free_fall(std::max(r_cool/r200, 1.0e-12), haloes::Halo::get_Mdm_tilde(std::max(r_cool/r200, 1.0e-12), concentration), m200, z))*this->params->fraction_t;
  			}else if(density_norm==0){
  				dt=(tf-t0)/10.0;
  			}


  			if (dt<(tf-t0)/1000.0){
  				dt=(tf-t0)/1000.0;
  			}else if(dt > (tf-t0)/10.0){
  				dt=(tf-t0)/10.0;
  			}

  			if (t+dt>tf){
  				dt=tf-t;
  			}

  			params[0]=&z;
  			params[1]=&t_cool_avail;
  			params[2]=&t_gas;
  			params[3]=&density_norm;

  			try{
	  			if (density_norm>0 && r_cool_solver(log(std::max(r_cool/r200, 1.0e-12)), &params)<0){
	  				
  					roots=helpers::NonLinearSolver::find_roots(&(Galaxy::r_cool_solver), &params, log(std::max(r_cool/r200, 1.0e-12)), 0.0, 1.0e-1, 100, 100);
  					
  					if (roots.size()>0){
  						r_cool_tmp=r200*exp(roots[0]);
  					}else{
  						if (r_cool_solver(0.0, &params)<0){
  							r_cool_tmp=r200;
  						}else{
  							r_cool_tmp=r_cool;
  						}
  					}
	  			}else{
	  				r_cool_tmp=r_cool;
	  			}
	  		}catch(const std::exception& e){
				printf("error msg: %s\n", e.what());
				throw "Error finding cooling radius!";
			}catch(alglib::ap_error e){
				printf("error msg: %s\n", e.msg.c_str());
				throw "Error finding cooling radius!";
			}
  			


  			if (r_cool_tmp>r_cool){
  				m_cold_new=density_norm*(haloes::Halo::get_Mb_tilde(r_cool_tmp/r200, concentration, t200/t_gas)-haloes::Halo::get_Mb_tilde(r_cool/r200, concentration, t200/t_gas));
  			
  				t_cool_avail=e_radiated/cool_luminosity(r_cool/r200, 1.0, m200, density_norm, concentration, t_gas, z, interp)+dt;
  				e_radiated+=cool_luminosity(r_cool/r200, 1.0, m200, density_norm, concentration, t_gas, z, interp)*dt - cool_luminosity(r_cool/r200, r_cool_tmp/r200, m200, density_norm, concentration, t_gas, z, interp)*t_cool_avail;
  			}else if (density_norm>0.0){
  				m_cold_new=0.0;

  				t_cool_avail=e_radiated/cool_luminosity(r_cool/r200, 1.0, m200, density_norm, concentration, t_gas, z, interp)+dt;
  				e_radiated+=cool_luminosity(r_cool/r200, 1.0, m200, density_norm, concentration, t_gas, z, interp)*dt;

  			}else{
  				m_cold_new=0.0;
  			}
  			if(r_cool_tmp>0.0){
  				r_cool=r_cool_tmp*std::max(0.0, std::min(1.0-dt/t_free_fall(r_cool_tmp/r200, haloes::Halo::get_Mdm_tilde(r_cool_tmp/r200, z), m200, z), 1.0));
  			}

  			cold_gas+=m200*m_cold_new;
  			hot_gas-=m200*m_cold_new;


  			if(stars){
  				make_stars_do_feedback(m200, z);
  			}

  			t+=dt;

  			counter++;
  		}
  	}

  	void Galaxy::make_stars_do_feedback(double m200, double z){
  		stellar_mass+=cold_gas*params->star_formation_ratio;
  		cold_gas-=cold_gas*params->star_formation_ratio;

  		double imf_norm=model::Cosmology::integrated_imf(params->max_stellar_mass, params->imf_slope)-model::Cosmology::integrated_imf(params->min_stellar_mass, params->imf_slope);

  		auto inverse_stellar_cdf = [](double r, double norm, double alpha, double min_stellar_mass){
  			return pow(r/norm*(1-alpha)+pow(min_stellar_mass, 1-alpha), 1/(1-alpha));
  		};
  		double stellar_mass_created=0.0;
  		int n_sn=0;
  		double r;
  		double mass;
  		while(stellar_mass_created<stellar_mass){
  			r = ((double) rand() / (RAND_MAX));
  			mass=inverse_stellar_cdf(r, imf_norm, params->imf_slope, params->min_stellar_mass);

  			stellar_mass_created+=mass;
  			if(mass>params->min_sn_mass){
  				n_sn++;
  			}

  		}

  		std::cout<<params->feedback_coupling * n_sn *params->sn_energy<<"; "<<haloes::Halo::get_binding_energy(m200, z)<<std::endl;

  		
  		if(params->feedback_coupling * n_sn *params->sn_energy > haloes::Halo::get_binding_energy(m200, z)){
  			ejected_gas+=hot_gas+cold_gas;
  			hot_gas=0.0;
  			cold_gas=0.0;
  		}

  	}
}