#ifndef Readers_H
#define Readers_H

namespace readers{
	class ReadMergerTree{
		private:
			haloes::Halo *root=NULL;

		public:
			ReadMergerTree(std::string hdf5_filename, int treeIndex);
			haloes::Halo* get_root();
	};
}

#endif