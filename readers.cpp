#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "interpolation.h"
#include <boost/math/special_functions/erf.hpp>
#include <fstream>
#include <sstream>
#include <iostream>
#include <string>
#include <iterator>
#include <algorithm>
#include "H5Cpp.h"
#include "helpers.hpp"
#include "haloes.hpp"
#include "model.hpp"
#include "readers.hpp"

using namespace H5;

namespace readers{

	ReadMergerTree::ReadMergerTree(std::string hdf5_filename, int treeIndex){
		H5File fp(hdf5_filename.c_str(),H5F_ACC_RDONLY);

		DataSet treeFirstIndexSet=fp.openDataSet("hostIndex/firstTree");
		DataSet branchNoSet=fp.openDataSet("hostIndex/numberOfTrees");
		DataSpace treeFirstIndexSpace = treeFirstIndexSet.getSpace();
		DataSpace branchNoSpace = branchNoSet.getSpace();

		int *treeFirstIndexSelect = new int;
		int *branchNoSelect = new int;

		hsize_t offset[1], count[1], stride[1], block[1];
		count[0]=1;
		offset[0]=treeIndex;
		stride[0]=1;
		block[0]=1;
		treeFirstIndexSpace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);
		branchNoSpace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);
		DataSpace memspace(1, count, NULL);

		treeFirstIndexSet.read(treeFirstIndexSelect, PredType::NATIVE_INT, memspace, treeFirstIndexSpace);
		branchNoSet.read(branchNoSelect, PredType::NATIVE_INT, memspace, branchNoSpace);

		std::cout<<*treeFirstIndexSelect<<std::endl;
		std::cout<<*branchNoSelect<<std::endl;

		DataSet branchesFirstNodeSet = fp.openDataSet("treeIndex/firstNode");
		DataSet branchesNoNodeSet = fp.openDataSet("treeIndex/numberOfNodes");
		DataSpace branchesFirstNodeSpace = branchesFirstNodeSet.getSpace();
		DataSpace branchesNoNodeSpace = branchesNoNodeSet.getSpace();

		int *branchFirstNodeSelect = new int[*branchNoSelect];
		int *branchNoNodeSelect = new int[*branchNoSelect];

		count[0]=*branchNoSelect;
		offset[0]=*treeFirstIndexSelect;
		stride[0]=1;
		block[0]=1;

		branchesFirstNodeSpace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);
		branchesNoNodeSpace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);
		memspace =DataSpace(1, count, NULL);

		branchesFirstNodeSet.read(branchFirstNodeSelect, PredType::NATIVE_LONG, memspace, branchesFirstNodeSpace);
		branchesNoNodeSet.read(branchNoNodeSelect, PredType::NATIVE_LONG, memspace, branchesNoNodeSpace);

		std::cout<<branchFirstNodeSelect[9]<<std::endl;
		std::cout<<branchNoNodeSelect[9]<<std::endl;

		DataSet nodeHostIndexSet = fp.openDataSet("haloTrees/hostIndex");
		DataSet descendantHostIndexSet = fp.openDataSet("haloTrees/descendantHost");
		DataSet nodeMassSet = fp.openDataSet("haloTrees/nodeMass");
		DataSet nodeRedshiftSet = fp.openDataSet("haloTrees/redshift");
		DataSpace nodeHostIndexSpace = nodeHostIndexSet.getSpace();
		DataSpace descendantHostIndexSpace = descendantHostIndexSet.getSpace();
		DataSpace nodeMassSpace = nodeMassSet.getSpace();
		DataSpace nodeRedshiftSpace = nodeRedshiftSet.getSpace();

		std::vector<int> nodeIDs;
		std::vector<int> descendantIDs;
		std::vector<haloes::Halo *> haloes;
		haloes::Halo *root;

		for (int i=0; i<*branchNoSelect; i++){
			count[0]=branchNoNodeSelect[i];
			offset[0]=branchFirstNodeSelect[i];
			stride[0]=1;
			block[0]=1;
			memspace =DataSpace(1, count, NULL);
			std::cout<<count[0]<<"; "<<offset[0]<<std::endl;

			int *nodeHostIndexSelect = new int[branchNoNodeSelect[i]];
			int *descendantHostIndexSelect = new int[branchNoNodeSelect[i]];
			double *nodeMassSelect = new double[branchNoNodeSelect[i]];
			double *nodeRedshiftSelect = new double[branchNoNodeSelect[i]];

			nodeHostIndexSpace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);
			descendantHostIndexSpace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);
			nodeMassSpace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);
			nodeRedshiftSpace.selectHyperslab(H5S_SELECT_SET, count, offset, stride, block);

			nodeHostIndexSet.read(nodeHostIndexSelect, PredType::NATIVE_INT, memspace, nodeHostIndexSpace);
			descendantHostIndexSet.read(descendantHostIndexSelect, PredType::NATIVE_INT, memspace, descendantHostIndexSpace);
			nodeMassSet.read(nodeMassSelect, PredType::NATIVE_DOUBLE, memspace, nodeMassSpace);
			nodeRedshiftSet.read(nodeRedshiftSelect, PredType::NATIVE_DOUBLE, memspace, nodeRedshiftSpace);


			std::cout<<nodeMassSelect[0]<<"; "<<nodeMassSelect[1]<<std::endl;

			for(int j=0; j<branchNoNodeSelect[i]; j++){
				haloes.push_back(new haloes::Halo(nodeMassSelect[j], nodeRedshiftSelect[j]));
				nodeIDs.push_back(nodeHostIndexSelect[j]);
				
				descendantIDs.push_back(descendantHostIndexSelect[j]);
			}
		}

		for(int i=0; i<nodeIDs.size(); i++){
			if(descendantIDs[i] != -1){
				int index=std::distance(nodeIDs.begin(), std::find(nodeIDs.begin(), nodeIDs.end(), descendantIDs[i]));
				haloes[i]->set_parent(haloes[index]);
			}else{
				root=haloes[i];
			}
		}

		std::cout<<root->get_most_massive_progenitor()<<std::endl;

	}

	haloes::Halo* ReadMergerTree::get_root(){
		return root;
	}

}