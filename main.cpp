#include "stdafx.h"
#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <time.h>
#include "interpolation.h"
#include "integration.h"
#include <boost/math/special_functions/erf.hpp>
#include <fstream>
#include <sstream>
#include <string>
#include <thread>
#include <iterator>
#include <random>
#include <algorithm>
#include "helpers.hpp"
#include "model.hpp"
#include "haloes.hpp"
#include "galaxies.hpp"
#include "readers.hpp"




void print_tree(haloes::Halo *halo, std::string tab_string){
	std::cout<<tab_string<<halo->get_mass()<<"; "<<halo->get_z()<<"{"<<std::endl;
	std::string new_tab_string=tab_string+"\t";
	for(haloes::Halo *p:halo->get_progenitors()){
		print_tree(p, new_tab_string);
	}
	std::cout<<tab_string<<"}"<<std::endl;

}


bool halo_can_cool(haloes::Halo *halo, helpers::Interpolator *interp, model::ModelParams *params){
	double e_cool=0.0;
	double dt;
	double temp;
	double luminosity;
	double t_cool_avail;
	double t_cool;

	std::vector<haloes::Halo *> history=halo->get_halo_history();
	for(int i=0; i<history.size()-1; i++){
		dt=interp->t_of_z(history[i+1]->get_z())-interp->t_of_z(history[i]->get_z());
		if(history[i]->get_z() > model::Cosmology::z_reion){
			temp=std::max(history[i]->get_T200(), model::Cosmology::T0_z(history[i]->get_z()));
		}else{
			temp=std::max(history[i]->get_T200(), params->t_gas_post_reion);
		}

		luminosity=galaxies::Galaxy::cool_luminosity(0.0, 1.0, history[i]->get_mass(), 1.0, history[i]->get_c(), temp, history[i]->get_z(), interp);
		t_cool_avail=e_cool/luminosity+dt;
		e_cool+=luminosity*dt;

		t_cool=galaxies::Galaxy::t_cool(temp, history[i]->rhoB_tilde_max(temp), history[i]->get_z(), interp);

		if(t_cool<=t_cool_avail){
			return true;
		}
	}
	return false;
}

double get_cool_fraction(std::vector<haloes::Halo *>* hs, helpers::Interpolator *interp, model::ModelParams *params){
	int sum=0;
	for(int i=0; i<hs->size(); i++){
		if(halo_can_cool(hs->at(i), interp, params)){
			sum++;
		}
	}

	return ((double) sum)/((double) hs->size());
	
}

//routines to generate a single halo merger tree
haloes::Halo* make_halo(int i, double mass, double z0, double minMass, double z_max, double deltaW, bool populateChildren, helpers::Interpolator *interp){
	haloes::Halo *h = new haloes::Halo(mass, z0, interp);
	h->populate_progenitors(deltaW, z_max, minMass, interp, populateChildren);
	return h;
}
//(uses jeans mass as limit)
haloes::Halo* make_halo(int i, double mass, double z0, double deltaW, bool populateChildren, helpers::Interpolator *interp){
	haloes::Halo *h = new haloes::Halo(mass, z0, interp);
	h->populate_progenitors(deltaW, interp, populateChildren);
	return h;
}

//routines to make n haloes of a given mass
std::vector<haloes::Halo *>* make_halo_sample(double mass, double z0, int n, double minMass, double z_max, double deltaW, bool populateChildren, helpers::Interpolator *interp){
	std::vector<haloes::Halo *> *hs = new std::vector<haloes::Halo *>();
	for(int i=0; i<n; i++){
		haloes::Halo *h = new haloes::Halo(mass, z0, interp);
		h->populate_progenitors(deltaW, z_max, minMass, interp, populateChildren);
		hs->push_back(h);
	}
	return hs;
}
//(uses jeans mass as limit)
std::vector<haloes::Halo *>* make_halo_sample(double mass, double z0, int n, double deltaW, bool populateChildren, helpers::Interpolator *interp){
	std::vector<haloes::Halo *> *hs = new std::vector<haloes::Halo *>();
	for(int i=0; i<n; i++){
		haloes::Halo *h = new haloes::Halo(mass, z0, interp);
		h->populate_progenitors(deltaW, interp, populateChildren);
		hs->push_back(h);
	}
	return hs;
}

//routine to populate a given halo
galaxies::Galaxy* make_populated_Galaxy(haloes::Halo *halo, helpers::Interpolator *interp, model::ModelParams *params) {
	galaxies::Galaxy *g = new galaxies::Galaxy(params, interp);
	g->populate_galaxies(halo);
	return g;
}

//routines to make multiple samples of different mass in parallel
std::vector<std::vector<haloes::Halo *>*> make_samples_parallel(int nThreads, std::vector<double> masses, double z0, int n, double minMass, double z_max, double deltaW, bool populateChildren, helpers::Interpolator *interp){
	return helpers::ParallelMapping::map_parallel<std::vector<haloes::Halo *>*, double, double, int, double, double, double, bool, helpers::Interpolator*>(&make_halo_sample, nThreads, &masses, z0, n, minMass, z_max, deltaW, populateChildren, interp);
}
//(uses jeans mass as limit)
std::vector<std::vector<haloes::Halo *>*> make_samples_parallel(int nThreads, std::vector<double> masses, double z0, int n, double deltaW, bool populateChildren, helpers::Interpolator *interp){
	return helpers::ParallelMapping::map_parallel<std::vector<haloes::Halo *>*, double, double, int, double, bool, helpers::Interpolator*>(&make_halo_sample, nThreads, &masses, z0, n, deltaW, populateChildren, interp);	
}

//routines to make n haloes of a given mass
std::vector<haloes::Halo*> make_one_sample_parallel(int nThreads, int n, double mass, double z0, double deltaW, bool populateChildren, helpers::Interpolator *interp){
	std::vector<int> ids;
	for(int i=0; i<n; i++){
		ids.push_back(i);
	}
	return helpers::ParallelMapping::map_parallel<haloes::Halo*, int, double, double, double, bool, helpers::Interpolator*>(&make_halo, nThreads, &ids, mass, z0, deltaW, populateChildren, interp);
}
//(uses jeans mass as limit)
std::vector<haloes::Halo*> make_one_sample_parallel(int nThreads, int n, double mass, double z0, double minMass, double z_max, double deltaW, bool populateChildren, helpers::Interpolator *interp){
	std::vector<int> ids;
	for(int i=0; i<n; i++){
		ids.push_back(i);
	}
	return helpers::ParallelMapping::map_parallel<haloes::Halo*, int, double, double, double, double, double, bool, helpers::Interpolator*>(&make_halo, nThreads, &ids, mass, z0, minMass, z_max, deltaW, populateChildren, interp);
}

//routine to get the fraction of haloes that cool given a list of samples
std::vector<double> get_cool_fraction_parallel(int nThreads, std::vector<std::vector<haloes::Halo *>*> halo_samples, helpers::Interpolator *interp, model::ModelParams *params){
	return helpers::ParallelMapping::map_parallel<double, std::vector<haloes::Halo *>*, helpers::Interpolator*, model::ModelParams*>(&get_cool_fraction, nThreads, &halo_samples, interp, params);
}

//routine to populate a given sample of haloes with galaxies in parallel
std::vector<galaxies::Galaxy *> populate_galaxies_parallel(int nThreads, std::vector<haloes::Halo *> haloes, helpers::Interpolator *interp, model::ModelParams *params){
	return helpers::ParallelMapping::map_parallel<galaxies::Galaxy*, haloes::Halo*, helpers::Interpolator*, model::ModelParams*>(&make_populated_Galaxy, nThreads, &haloes, interp, params);
}



int main(int argc, char **argv)
{
	srand (time(NULL));

	helpers::Interpolator *interp = new helpers::Interpolator("power_spectrum.dat", "../UV_dust1_CR1_G1_shield1.hdf5");
	model::ModelParams *params = new model::ModelParams();

	std::vector<haloes::Halo *> *sample = make_halo_sample(1.0e6, 0.0, 1000, 0.1, false, interp);
	double cooled_fraction= get_cool_fraction(sample, interp, params);
	std::cout<<"Mean: "<<cooled_fraction<<std::endl;


	/*
	std::vector<double> inputs;
	double mass=1.0e4;
	do{
		inputs.push_back(mass);
		mass*=3.33;
	}while(mass<1.0e9);
	std::cout<<"Input size: "<<inputs.size()<<std::endl;

	std::vector<std::vector<haloes::Halo *>*> test = helpers::ParallelMapping::map_parallel<std::vector<haloes::Halo *>*, double, double, int, double, bool, helpers::Interpolator*>(&make_halo_sample, 4, &inputs, 0.0, 1000, 0.1, false, interp);
	std::cout<<test.size()<<std::endl;
	std::cout<<test.at(0)->at(0)->get_mah()[0]<<std::endl;
	std::cout<<test.at(1)->at(0)->get_mass()<<std::endl;
	std::cout<<test.at(2)->at(0)->get_mass()<<std::endl;
	std::cout<<test.at(3)->at(0)->get_mass()<<std::endl;
	




	/*
	

	std::cout<<haloes::Halo::mass_concentration_relation(1.0e5, 20.0, interp)<<std::endl;

	
	haloes::Halo *halo = new haloes::Halo(1.0e12, 0.0, interp);
	halo->populate_progenitors(0.3, 35.0, 5.0e5, interp, true, false);
	galaxies::Galaxy *g = new galaxies::Galaxy(params, interp);
	g->populate_galaxies(halo);
	
	std::vector<galaxies::Galaxy *> sats =g->get_satellites();
	std::cout<<"# of satellites: "<<sats.size()<<std::endl;


	
	std::cout << "number of leafs: "<< halo->get_leafs().size()<<std::endl;
	std::vector<haloes::Halo *> leafs=halo->get_leafs();
	std::sort(leafs.begin(), leafs.end(), [](haloes::Halo * lhs, haloes::Halo * rhs) {
		if(lhs->get_z() > rhs->get_z()){
			return true;
		}else if(lhs->get_z() > rhs->get_z()){
			return (lhs->get_mass() > rhs->get_mass());
		}else{
			return false;
		}
	});

	int num_pre_reion_sat=0;
	for(int i=0; i< sats.size(); i++){
		if (sats[i]->get_z_became_satellite()>model::Cosmology::z_reion){
			num_pre_reion_sat++;
		}
	}

	std::cout<<"There are "<<num_pre_reion_sat<<" satellites that became satellites before reionization ("<<((double)num_pre_reion_sat)/sats.size()*100<<"%)."<<std::endl;

	/*
	//print_tree(halo, "");


	haloes::Halo *leaf=leafs[0]->get_parent()->get_parent();

	
	galaxies::Galaxy *g2 = new galaxies::Galaxy(params, interp);
	g2->populate_galaxies_wStars(leaf);
	std::cout<<g2->get_cold_gas()/(g2->get_cold_gas()+g2->get_hot_gas())<<std::endl;
	std::cout<<(g2->get_cold_gas()+g2->get_hot_gas())/g2->halo_dm_mass()<<std::endl;

	double sum_cold_gas=g2->get_cold_gas();
	std::vector<galaxies::Galaxy *> sats=g2->get_satellites();
	std::cout<<"# of satellites: "<<sats.size()<<std::endl;
	for(int i=0; i<sats.size(); i++){
		sum_cold_gas+=sats[i]->get_cold_gas();
	}
	std::cout<<(sum_cold_gas+g2->get_hot_gas())/g2->halo_dm_mass()<<std::endl;
	*/
	return 0;


}

