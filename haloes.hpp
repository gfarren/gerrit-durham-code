#ifndef Haloes_H
#define Haloes_H

namespace haloes{

	class Halo{

		private: 
			double m,z,c;
			std::vector<Halo *> progenitors = std::vector<Halo *>();
			Halo* most_massive_progenitor = NULL;
			Halo* parent;
			static double ps_cdf_inverse(double r, double deltaW);
			static double delta_c(double c);
			static void mB_integrant(double x, double xminusa, double bminusx, double &y, void *ptr);
			


		public:
			Halo(double mass, double z_in, Halo *parent, helpers::Interpolator *interp);
			Halo(double mass, double z_in, helpers::Interpolator *interp);
			Halo(double mass, double z_in, Halo *parent);
			Halo(double mass, double z_in);
			void populate_progenitors(double deltaW, double max_z, double min_mass, helpers::Interpolator *interp, bool populateChildren, bool useJeansMass);
			void populate_progenitors(double deltaW, helpers::Interpolator *interp, bool populateChildren){
				populate_progenitors(deltaW, 50.0, 1.0e5, interp, populateChildren, true);
			}
			void populate_progenitors(double deltaW, double max_z, double min_mass, helpers::Interpolator *interp, bool populateChildren){
				populate_progenitors(deltaW, max_z, min_mass, interp, populateChildren, false);
			}

			double get_mass(){ return m; }
			double get_z(){ return z; }
			double get_c(){return c;}
			std::vector<Halo *> get_halo_history();
			std::vector<double> get_mah();
			std::vector<double> get_z_mah();
			std::vector<Halo *> get_progenitors(){return progenitors;}
			Halo* get_most_massive_progenitor(){ return this->most_massive_progenitor;}
			Halo* get_parent(){return this->parent;}
			std::vector<Halo *> get_leafs();
			double get_smooth_mass_growth();
			double get_life_time();
			void set_parent(Halo *parent);
			void add_progenitor(Halo *progenitor);

			static double get_T200(double m200, double z, double mu=1.0);
			static double get_R200(double m200, double z);
			static double get_V200(double m200, double z);
			static double get_binding_energy(double m200, double z);

			static double get_T200(Halo *halo){
				return get_T200(halo->get_mass(), halo->get_z());
			}
			static double get_R200(Halo *halo){
				return get_R200(halo->get_mass(), halo->get_z());
			}
			static double get_V200(Halo *halo){
				return get_V200(halo->get_mass(), halo->get_z());
			}
			static double get_binding_energy(Halo *halo){
				return get_binding_energy(halo->get_mass(), halo->get_z());
			}

			double get_T200(){
				return get_T200(this->get_mass(), this->get_z());
			}
			double get_R200(){
				return get_R200(this->get_mass(), this->get_z());
			}
			double get_V200(){
				return get_V200(this->get_mass(), this->get_z());
			}
			double get_binding_energy(){
				return get_binding_energy(this->get_mass(), this->get_z());
			}

			static double get_Mdm_tilde(double r, double c);
			static double get_rhoB_tilde(double r, double c, double temp_ratio=1.0); //after reionization need to be able to set halo temperature (solution depends on the ration of virial temperature to halo temperature)
			static double get_rhoB_tilde_max(double c, double temp_ratio=1.0); //after reionization need to be able to set halo temperature (solution depends on the ration of virial temperature to halo temperature)
			static double get_Mb_tilde(double r, double c, double temp_ratio=1.0);

			double Mdm_tilde(double r){
				return get_Mdm_tilde(r, this->get_c());
			}
			double rhoB_tilde(double r, double t_gas){
				return get_rhoB_tilde(r, this->get_c(), this->get_T200()/t_gas);
			}
			double rhoB_tilde(double r){
				return get_rhoB_tilde(r, this->get_c());
			}
			double rhoB_tilde_max(double t_gas){
				return get_rhoB_tilde_max(this->get_c(), this->get_T200()/t_gas);
			}
			double rhoB_tilde_max(){
				return get_rhoB_tilde_max(this->get_c());
			}
			double Mb_tilde(double r){
				return get_Mb_tilde(r, get_c());
			}
			double Mb_tilde(double r, double t_gas){
				return get_Mb_tilde(r, get_c(), this->get_T200()/t_gas);
			}
			static double mass_concentration_relation_1(double m200, double z, helpers::Interpolator *interp);
			static double mass_concentration_relation_2(double m200, double z);


	};
}
#endif