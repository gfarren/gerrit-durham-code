
# distutils: language = c++


from libcpp.vector cimport vector
from libcpp.string cimport string
from cpython.mem cimport PyMem_Malloc, PyMem_Free
from libc.string cimport memcpy

cdef extern from "model.cpp":
	pass
cdef extern from "haloes.cpp":
	pass
cdef extern from "helpers.cpp":
	pass
cdef extern from "galaxies.cpp":
	pass

#from libcpp.string cimport bool

cdef extern from "helpers.hpp" namespace "helpers":
	cdef cppclass Interpolator:
		Interpolator(double, double , int) except +
		Interpolator(double, double , int, double, double, int, string, string, bint) except +
		Interpolator(double, double , int, double, double, int, string) except +
		Interpolator(string, string, bint) except +
		Interpolator(string) except +

		double get_sigma_sq_inverse(double)#sigma_sq
		double get_sigma_sq(double)#mass
		double get_sigma_sq(double, double)#mass, z
		double d_of_z(double)#z
		double z_of_d(double)#d
		double t_of_z(double)#z
		double z_of_t(double)#t
		double get_power_spectrum(double)#k
		double get_maxk()
		double get_mink()
		double get_cooling_rate(double, double, double)#rho, t_gas, z


cdef extern from "model.hpp" namespace "model":
	cdef cppclass ModelParams:
		ModelParams() except +

cdef extern from "haloes.hpp" namespace "haloes":
	cdef cppclass Halo:
		Halo(double, double, Halo*, Interpolator*) except +
		Halo(double, double, Interpolator*) except +
		void populate_progenitors(double, double, double, Interpolator*, bint, bint)
		double get_mass()
		double get_z()
		double get_c()
		vector[Halo*] get_halo_history()
		vector[double] get_mah()
		vector[double] get_z_mah()
		vector[Halo*] get_progenitors()
		Halo* get_most_massive_progenitor()
		Halo* get_parent()
		vector[Halo*] get_leafs()

		double get_T200()
		double get_R200()
		double get_V200()
		double get_binding_energy()

		double Mdm_tilde(double)
		double rhoB_tilde(double, double)
		double rhoB_tilde_max(double)
		double Mb_tilde(double, double)

cdef extern from "galaxies.hpp" namespace "galaxies":
	cdef cppclass Galaxy:
		Galaxy(ModelParams*, Interpolator*) except +
		double get_merged_dm_mass()
		double halo_dm_mass()

		vector[Galaxy *] get_satellites()
		int get_num_satellites()
		vector[Galaxy *] get_merged_satellites()

		double get_z_became_satellite()
		double get_z_will_merge()

		double get_cold_gas()
		double get_hot_gas()
		double get_stellar_mass()

		bint get_is_satellite()
		bint get_is_merged()

		void populate_galaxies(Halo *)

ctypedef vector[Halo *]* halo_pointer_vector_pointer

cdef extern from "galaxies.hpp" namespace "galaxies::Galaxy":
	double t_cool(double, double, double, Interpolator*)#t_gas, rho_tilde, z, interpolator
	double t_free_fall(double, double, double, double)#r0, m_enclosed_tilde, m200,z
	double cool_luminosity(double, double, double, double, double, double, double, Interpolator*) #rmin, rmax, m200, rho_norm, concentration, t_gas, z, interpolator 

cdef extern from "haloes.hpp" namespace "haloes::Halo":
	double mass_concentration_relation_1(double, double, Interpolator*)#m200, z, interpolator
	double mass_concentration_relation_2(double, double)#m200, z, interpolator

cdef extern from "main.cpp":
	double get_cool_fraction(vector[Halo*]*, Interpolator*, ModelParams*)

	Halo* make_halo(int, double, double, double, double, double, bint, Interpolator*)
	Halo* make_halo(int, double, double, double, bint, Interpolator*)

	halo_pointer_vector_pointer make_halo_sample(double, double, int, double, double, double, bint, Interpolator*)
	halo_pointer_vector_pointer make_halo_sample(double, double, int, double, bint, Interpolator*)

	Galaxy* make_populated_Galaxy(Halo*, Interpolator*, ModelParams*)

	vector[Halo*] make_one_sample_parallel(int, int, double, double, double, bint, Interpolator*)
	vector[Halo*] make_one_sample_parallel(int, int, double, double, double, double, double, bint, Interpolator*)

	vector[halo_pointer_vector_pointer] make_samples_parallel(int, vector[double], double, int, double, double, double, bint, Interpolator*)
	vector[halo_pointer_vector_pointer] make_samples_parallel(int, vector[double], double, int, double, bint, Interpolator*)

	vector[double] get_cool_fraction_parallel(int, vector[halo_pointer_vector_pointer], Interpolator*, ModelParams*)

	vector[Galaxy *] populate_galaxies_parallel(int, vector[Halo*], Interpolator*, ModelParams*)



cdef class PyInterpolator:
	cdef Interpolator *interp
	def __cinit__(self, double minZ=0.0, double maxZ=60.0, int stepsZ=1000, double minM=1.0, double maxM=1.0e15, int stepsM=1000, string power_filename="", string cooling_filename="", bint molecules=True):
		if power_filename =="" and cooling_filename=="":
			self.interp=new Interpolator(minZ, maxZ, stepsZ)
		elif power_filename !="" and cooling_filename=="":
			self.interp=new Interpolator(minM, maxM, stepsM, minZ, maxZ, stepsZ, power_filename)
		else:
			self.interp=new Interpolator(minM, maxM, stepsM, minZ, maxZ, stepsZ, power_filename, cooling_filename, molecules)

	def get_sigma_sq_inverse(self, double sigma_sq):
		return self.interp.get_sigma_sq_inverse(sigma_sq)
	def get_sigma_sq(self, double mass, double z=0.0):
		return self.interp.get_sigma_sq(mass, z)
	def d_of_z(self, double z):
		return self.interp.d_of_z(z)
	def z_of_d(self, double d):
		return self.interp.z_of_d(d)
	def t_of_z(self, double z):
		return self.interp.t_of_z(z)
	def z_of_t(self, double t):
		return self.interp.z_of_t(t)
	def get_power_spectrum(self, double k):
		return self.interp.get_power_spectrum(k)
	def get_maxk(self):
		return self.interp.get_maxk()
	def get_mink(self):
		return self.interp.get_mink()
	def get_cooling_rate(self, double rho, double t_gas, double z):
		return self.interp.get_cooling_rate(rho, t_gas, z)


cdef class PyModelParams:
	cdef ModelParams *params 
	def __cinit__(self, ):
		self.params=new ModelParams()

cdef class PyGalaxy:
	cdef Galaxy *gal
	cdef public list py_satellites, py_merged_gals

	def __cinit__(self, PyInterpolator interp, PyModelParams params = None):
		if params is None:
			params = PyModelParams()

		if params is None and interp is None:
			self.gal=NULL
		else:
			self.gal=new Galaxy(params.params, interp.interp)

		self.py_satellites=[]
		self.py_merged_gals=[]

	def __dealloc__(self):
		del self

	def get_merged_dm_mass(self):
		return self.gal.get_merged_dm_mass()

	def halo_dm_mass(self):
		return self.gal.halo_dm_mass()

	def get_satellites(self):
		if len(self.py_satellites)==0 and self.gal.get_satellites().size()>0:
			for i in range(self.gal.get_satellites().size()):
				p=PyGalaxy(None, None)
				p.gal=self.gal.get_satellites()[i]
				self.py_satellites.append(p)
		return self.py_satellites
	def get_num_satellites(self):
		self.gal.get_num_satellites()

	def get_merged_satellites(self):
		if len(self.py_merged_gals)==0 and self.gal.get_merged_satellites().size()>0:
			for i in range(self.gal.get_merged_satellites().size()):
				p=PyGalaxy(None, None)
				p.gal=self.gal.get_merged_satellites()[i]
				self.py_merged_gals.append(p)
		return self.py_merged_gals

	def get_z_became_satellite(self):
		return self.gal.get_z_became_satellite()

	def get_z_will_merge(self):
		return self.gal.get_z_will_merge()

	def get_cold_gas(self):
		return self.gal.get_cold_gas()

	def get_hot_gas(self):
		return self.gal.get_hot_gas()

	def get_stellar_mass(self):
		return self.gal.get_stellar_mass()

	def get_is_satellite(self):
		return self.gal.get_is_satellite()

	def get_is_merged(self):
		return self.gal.get_is_merged()

	def populate_galaxies(self, PyHalo parent):
		self.gal.populate_galaxies(parent.halo)
		self.py_satellites=[]
		self.py_merged_gals=[]

	@staticmethod
	def get_t_cool(double t_gas, double rho_b_tilde, double z, PyInterpolator interp):#t_gas, rho_tilde, z, interpolator
		return t_cool(t_gas, rho_b_tilde, z, interp.interp)

	@staticmethod
	def get_t_free_fall(double r0, double m_enclosed_tilde, double m200, double z):#r0, m_enclosed_tilde, m200,z
		return t_free_fall(r0, m_enclosed_tilde, m200, z)

	@staticmethod
	def get_cool_luminosity(double rmin, double rmax, double m200, double rho_norm, double concentration, double t_gas, double z, PyInterpolator interp):
		return cool_luminosity(rmin, rmax, m200, rho_norm, concentration, t_gas, z, interp.interp)


cdef class PyHalo:
	cdef Halo *halo
	cdef public list py_progenitors,leafs, py_halo_history
	cdef PyHalo parent,mmp
	cdef bint is_pickled
	def __cinit__(self, double mass, double z_in, PyHalo parent_halo, PyInterpolator interp, makeHalo=True):
		if makeHalo:
			if parent_halo is None:
				self.parent=None
				self.halo=new Halo(mass, z_in, interp.interp)
			else:
				self.parent=parent_halo
				self.halo=new Halo(mass, z_in, parent_halo.halo, interp.interp)
		else:
			pass
		self.py_progenitors=[]
		self.leafs=[]
		self.py_halo_history=[]
		self.mmp=None
		self.is_pickled=False

	def __reduce__(self):
		#print "has been pickled"
		self.is_pickled=True
		return (rebuild, (<bytes>(<char *>self.halo)[:sizeof(Halo)], ))

	def __dealloc__(self):
		del self

	def populate_progenitors(self, double deltaW, double max_z, double min_mass, PyInterpolator interp, bint populateChildren=True, bint useJeansMass=False):
		self.halo.populate_progenitors(deltaW, max_z, min_mass, interp.interp, populateChildren, useJeansMass)
		self.mmp=None
		self.get_most_massive_progenitor()
		self.py_progenitors=[]
		self.get_progenitors()
		self.leafs=[]
		self.py_halo_history=[]


	def get_mass(self):
		return self.halo.get_mass()

	def get_z(self):
		return self.halo.get_z()

	def get_c(self):
		return self.halo.get_c()

	def get_halo_history(self):
		if len(self.py_halo_history)==0:
			history_tmp=self.halo.get_halo_history()
			for i in range(history_tmp.size()):
				p=PyHalo(0.0, 0.0, None, None, False)
				p.halo=history_tmp[i]
				self.py_halo_history.append(p)

		return self.py_halo_history

	def get_mah(self):
		return list(self.halo.get_mah())

	def get_z_mah(self):
		return list(self.halo.get_z_mah())

	def get_progenitors(self):
		if len(self.py_progenitors)==0 and self.halo.get_progenitors().size()>0:
			for i in range(self.halo.get_progenitors().size()):
				p=PyHalo(0.0, 0.0, self, None, False)
				p.halo=self.halo.get_progenitors()[i]
				self.py_progenitors.append(p)
		return self.py_progenitors

	def get_most_massive_progenitor(self):
		#print self.mmp,bool(self.halo.get_most_massive_progenitor() != NULL)
		if not self.mmp is None:
			return self.mmp
		elif bool(self.halo.get_most_massive_progenitor() != NULL):
			self.mmp=PyHalo(0.0,0.0, self, None, False)
			self.mmp.halo=self.halo.get_most_massive_progenitor()
			return self.mmp
		else:
			return None

	def get_parent(self):
		#print self.parent,bool(self.halo.get_parent() != NULL)
		if not self.parent is None:
			return self.parent
		elif bool(self.halo.get_parent() != NULL):
			self.parent=PyHalo(0.0,0.0, None, None, False)
			self.parent.halo=self.halo.get_parent()
			return self.parent
		else:
			None

	def get_leafs(self):
		
		if len(self.leafs)==0:
			leafs_temp=self.halo.get_leafs()
			for i in range(leafs_temp.size()):
				p=PyHalo(0.0, 0.0, None, None, False)
				p.halo=leafs_temp[i]
				self.leafs.append(p)

		return self.leafs

	def get_Mdm_tilde(self, double r):
		return self.halo.Mdm_tilde(r)

	def get_rhoB_tilde(self, double r, double t_gas=-1.0):
		if t_gas<0:
			t_gas=self.halo.get_T200()
		return self.halo.rhoB_tilde(r, t_gas)

	def get_rhoB_tilde_max(self, double t_gas=-1.0):
		if t_gas<0:
			t_gas=self.halo.get_T200()
		return self.halo.rhoB_tilde_max(t_gas)

	def get_Mb_tilde(self, double r, double t_gas=-1.0):
		if t_gas<0:
			t_gas=self.halo.get_T200()
		return self.halo.Mb_tilde(r, t_gas)

	def get_T200(self):
		return self.halo.get_T200()
	def get_R200(self):
		return self.halo.get_R200()
	def get_V200(self):
		return self.halo.get_V200()
	def get_binding_energy(self):
		return self.halo.get_binding_energy()

	@staticmethod
	def get_mass_concentration_relation_1(double m200, double z, PyInterpolator interp):
		return mass_concentration_relation_1(m200, z, interp.interp)
	@staticmethod	
	def get_mass_concentration_relation_2(double m200, double z):
		return mass_concentration_relation_2(m200, z)



cpdef PyHalo rebuild(bytes bytes_halo_pointer):
	h=PyHalo(0.0, 0.0, None, None, False)
	h.halo=<Halo*>PyMem_Malloc(sizeof(Halo))
	if not h.halo:
		raise MemoryError()
	memcpy(h.halo, <char*>bytes_halo_pointer,sizeof(Halo))
	return h

cdef class CoolingFractions:
	cdef vector[halo_pointer_vector_pointer] samples
	cdef int threads
	def __cinit__(self, int nThreads):
		self.threads=nThreads

	def make_samples(self, vector[double] masses, double z0, int n, double deltaW, PyInterpolator interp, bint populateChildren=False, double minMass=-1.0, double max_z=-1.0):
		if minMass<0 or max_z<0:
			self.samples = make_samples_parallel(self.threads, masses, z0, n, deltaW, populateChildren, interp.interp)
		else:
			self.samples = make_samples_parallel(self.threads, masses, z0, n, minMass, max_z, deltaW, populateChildren, interp.interp)

	def cooled_fraction(self, PyInterpolator interp, PyModelParams params):
		return get_cool_fraction_parallel(self.threads, self.samples, interp.interp, params.params)

	def get_samples(self):
		py_samples=[]
		cdef halo_pointer_vector_pointer sample
		for i in range(self.samples.size()):
			sample=self.samples.at(i)
			py_sample=[]
			for j in range(sample.size()):
				p=PyHalo(0.0, 0.0, None, None, False)
				p.halo = sample.at(j)
				py_sample.append(p)
			py_samples.append(py_sample)
		return py_samples

cdef class SatellitePopulation:
	cdef vector[Halo*] sample
	cdef vector[Galaxy*] galaxies
	cdef int threads
	def __cinit__(self, int nThreads):
		self.threads=nThreads

	def make_sample(self, double mass, double z0, int n, double deltaW, PyInterpolator interp, bint populateChildren=True, double minMass=-1.0, double max_z=-1.0):
		if minMass<0 or max_z<0:
			self.sample = make_one_sample_parallel(self.threads, n, mass, z0, deltaW, populateChildren, interp.interp)
		else:
			self.sample = make_one_sample_parallel(self.threads, n, mass, z0, minMass, max_z, deltaW, populateChildren, interp.interp)

	def make_populations(self, PyInterpolator interp, PyModelParams params):
		self.galaxies=populate_galaxies_parallel(self.threads, self.sample, interp.interp, params.params)

	def get_satellite_populations(self):
		num_satellites=[]
		cdef Galaxy *g
		for i in range(self.galaxies.size()):
			g=self.galaxies.at(i)
			num_satellites.append(g.get_num_satellites())

		return num_satellites

	def get_sample(self):
		py_sample=[]
		for i in range(self.sample.size()):
			p=PyHalo(0.0, 0.0, None, None, False)
			p.halo = self.sample.at(i)
		return py_sample
